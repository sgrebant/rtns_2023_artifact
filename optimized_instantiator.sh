#!/bin/bash

# Compiler setup
ARCH="arm-linux-gnueabi-"
CC=${ARCH}gcc
LD=${ARCH}ld
CFLAGS="-O3 -fno-stack-protector -static"
LDFLAGS="-z noexecstack"

# Base configuration
DIR="$(pwd)"
COMPILER=$DIR/compiler
FORMULASDIR=$DIR/experiments/formulas
OPTINSTS=$DIR/experiments/optimized_instantiators

OLD_PATH=$PATH
export PATH=$PATH:$DIR/otawa/bin

mkdir -p $OPTINSTS

INSTWCET=$DIT/experiments/instantiatiors_WCET.log

rm -f $INSTWCET

# The function that manage compilation of a formula file into an executable file
compile () {
    FORMULA=$(echo "$1" | sed 's/\.pwf//g')
    FORMULA=$(echo "$FORMULA" | sed 's/_BEFORE//g')

    # Generate the py file to compile
    $DIR/WSymb/simplify/swymplify -o $OPTINSTS/$FORMULA.py -c -p $FORMULASDIR/${FORMULA}_BEFORE.pwf

    # Compile to optimized C
    sed -i 's/..\/code_generator/compiler/g' $OPTINSTS/$FORMULA.py
    sed -i 's/import generate/import compiler/g' $OPTINSTS/$FORMULA.py
    python3 $OPTINSTS/$FORMULA.py > $OPTINSTS/$FORMULA.c

    # Use GCC to build the executable
    $CC $CFLAGS -o $OPTINSTS/$FORMULA.elf $COMPILER/test.c $OPTINSTS/$FORMULA.c
    WCET=$(owcet -s cmp_pipe -p pipeline=$DIR/WSymb/hw/processor.xml -p cache=$DIR/WSymb/hw/cache.xml $OPTINSTS/$FORMULA.elf eval 2> /dev/null)
    echo "$FORMULA -> $WCET"
}

# Setup
cd $FORMULASDIR
FORMULAS=$(ls *BEFORE.pwf)
cd $DIR

# Compile all formulas
for f in $FORMULAS; do
    compile $f
done

export PATH=$OLD_PATH
