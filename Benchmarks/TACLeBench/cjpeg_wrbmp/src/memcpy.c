/**
 * Custom memcpy implementation, to force gcc to compile without the standard library
 *
 * AUTHOR : Sandro Grebant
 */

#include "memcpy.h"

void *memcpy(void *dest, const void *src, size_t n){
	
	// parse src and dest into bytes
	BYTE* src_bytes = (BYTE*) src;
	BYTE* dest_bytes = (BYTE*) dest;

	// copy every src bytes into dest bytes
	for (int i=0; i<n; i++)
		dest_bytes[i] = src_bytes[i];
}
