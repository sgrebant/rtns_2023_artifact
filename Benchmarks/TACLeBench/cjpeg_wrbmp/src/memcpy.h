/**
 * Custom memcpy implementation, to force gcc to compile without the standard library
 *
 * AUTHOR : Sandro Grebant
 */

#ifndef MEMCPY
#define MEMCPY

/**
 * We can define a byte as a char, since the size of a char is one byte
 */
#define BYTE char

/**
 * According to Wikipedia, size_t is an unsigned int
 * Source : https://en.wikipedia.org/wiki/C_data_types#stddef.h
 */
typedef unsigned int size_t;

/**
 * memcpy implementation
 * @param dest the place where you want to copy
 * @param src the data you want to copy
 * @param n the number of bytes to copy
 */
void *memcpy(void *dest, const void *src, size_t n);

#endif
