
minver.elf:     file format elf32-littlearm


Disassembly of section .text:

00008000 <minver_fabs>:
    8000:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8004:	e28db000 	add	fp, sp, #0
    8008:	e24dd014 	sub	sp, sp, #20
    800c:	ed0b0b05 	vstr	d0, [fp, #-20]	; 0xffffffec
    8010:	ed5b0b05 	vldr	d16, [fp, #-20]	; 0xffffffec
    8014:	eef50bc0 	vcmpe.f64	d16, #0.0
    8018:	eef1fa10 	vmrs	APSR_nzcv, fpscr
    801c:	ba000004 	blt	8034 <minver_fabs+0x34>
    8020:	e24b3014 	sub	r3, fp, #20
    8024:	e893000c 	ldm	r3, {r2, r3}
    8028:	e50b200c 	str	r2, [fp, #-12]
    802c:	e50b3008 	str	r3, [fp, #-8]
    8030:	ea000002 	b	8040 <minver_fabs+0x40>
    8034:	ed5b0b05 	vldr	d16, [fp, #-20]	; 0xffffffec
    8038:	eef10b60 	vneg.f64	d16, d16
    803c:	ed4b0b03 	vstr	d16, [fp, #-12]
    8040:	e24b300c 	sub	r3, fp, #12
    8044:	e893000c 	ldm	r3, {r2, r3}
    8048:	ec432b30 	vmov	d16, r2, r3
    804c:	eeb00b60 	vmov.f64	d0, d16
    8050:	e28bd000 	add	sp, fp, #0
    8054:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    8058:	e12fff1e 	bx	lr

0000805c <minver_mmul>:
    805c:	e52db004 	push	{fp}		; (str fp, [sp, #-4]!)
    8060:	e28db000 	add	fp, sp, #0
    8064:	e24dd034 	sub	sp, sp, #52	; 0x34
    8068:	e50b0028 	str	r0, [fp, #-40]	; 0xffffffd8
    806c:	e50b102c 	str	r1, [fp, #-44]	; 0xffffffd4
    8070:	e50b2030 	str	r2, [fp, #-48]	; 0xffffffd0
    8074:	e50b3034 	str	r3, [fp, #-52]	; 0xffffffcc
    8078:	e51b3028 	ldr	r3, [fp, #-40]	; 0xffffffd8
    807c:	e50b3020 	str	r3, [fp, #-32]	; 0xffffffe0
    8080:	e51b3034 	ldr	r3, [fp, #-52]	; 0xffffffcc
    8084:	e50b3024 	str	r3, [fp, #-36]	; 0xffffffdc
    8088:	e51b3020 	ldr	r3, [fp, #-32]	; 0xffffffe0
    808c:	e3530000 	cmp	r3, #0
    8090:	da000009 	ble	80bc <minver_mmul+0x60>
    8094:	e51b3030 	ldr	r3, [fp, #-48]	; 0xffffffd0
    8098:	e3530000 	cmp	r3, #0
    809c:	da000006 	ble	80bc <minver_mmul+0x60>
    80a0:	e51b3024 	ldr	r3, [fp, #-36]	; 0xffffffdc
    80a4:	e3530000 	cmp	r3, #0
    80a8:	da000003 	ble	80bc <minver_mmul+0x60>
    80ac:	e51b202c 	ldr	r2, [fp, #-44]	; 0xffffffd4
    80b0:	e51b3030 	ldr	r3, [fp, #-48]	; 0xffffffd0
    80b4:	e1520003 	cmp	r2, r3
    80b8:	0a000001 	beq	80c4 <minver_mmul+0x68>
    80bc:	e59f312c 	ldr	r3, [pc, #300]	; 81f0 <minver_mmul+0x194>
    80c0:	ea000046 	b	81e0 <minver_mmul+0x184>
    80c4:	e3a03000 	mov	r3, #0
    80c8:	e50b3008 	str	r3, [fp, #-8]
    80cc:	ea00003e 	b	81cc <minver_mmul+0x170>
    80d0:	e3a03000 	mov	r3, #0
    80d4:	e50b300c 	str	r3, [fp, #-12]
    80d8:	ea000034 	b	81b0 <minver_mmul+0x154>
    80dc:	e3a02000 	mov	r2, #0
    80e0:	e3a03000 	mov	r3, #0
    80e4:	e50b201c 	str	r2, [fp, #-28]	; 0xffffffe4
    80e8:	e50b3018 	str	r3, [fp, #-24]	; 0xffffffe8
    80ec:	e3a03000 	mov	r3, #0
    80f0:	e50b3010 	str	r3, [fp, #-16]
    80f4:	ea00001a 	b	8164 <minver_mmul+0x108>
    80f8:	e59f10f4 	ldr	r1, [pc, #244]	; 81f4 <minver_mmul+0x198>
    80fc:	e51b2008 	ldr	r2, [fp, #-8]
    8100:	e1a03002 	mov	r3, r2
    8104:	e1a03083 	lsl	r3, r3, #1
    8108:	e0833002 	add	r3, r3, r2
    810c:	e51b2010 	ldr	r2, [fp, #-16]
    8110:	e0833002 	add	r3, r3, r2
    8114:	e1a03183 	lsl	r3, r3, #3
    8118:	e0813003 	add	r3, r1, r3
    811c:	edd31b00 	vldr	d17, [r3]
    8120:	e59f10d0 	ldr	r1, [pc, #208]	; 81f8 <minver_mmul+0x19c>
    8124:	e51b2010 	ldr	r2, [fp, #-16]
    8128:	e1a03002 	mov	r3, r2
    812c:	e1a03083 	lsl	r3, r3, #1
    8130:	e0833002 	add	r3, r3, r2
    8134:	e51b200c 	ldr	r2, [fp, #-12]
    8138:	e0833002 	add	r3, r3, r2
    813c:	e1a03183 	lsl	r3, r3, #3
    8140:	e0813003 	add	r3, r1, r3
    8144:	edd30b00 	vldr	d16, [r3]
    8148:	ee610ba0 	vmul.f64	d16, d17, d16
    814c:	ed5b1b07 	vldr	d17, [fp, #-28]	; 0xffffffe4
    8150:	ee710ba0 	vadd.f64	d16, d17, d16
    8154:	ed4b0b07 	vstr	d16, [fp, #-28]	; 0xffffffe4
    8158:	e51b3010 	ldr	r3, [fp, #-16]
    815c:	e2833001 	add	r3, r3, #1
    8160:	e50b3010 	str	r3, [fp, #-16]
    8164:	e51b2010 	ldr	r2, [fp, #-16]
    8168:	e51b3030 	ldr	r3, [fp, #-48]	; 0xffffffd0
    816c:	e1520003 	cmp	r2, r3
    8170:	baffffe0 	blt	80f8 <minver_mmul+0x9c>
    8174:	e59f1080 	ldr	r1, [pc, #128]	; 81fc <minver_mmul+0x1a0>
    8178:	e51b2008 	ldr	r2, [fp, #-8]
    817c:	e1a03002 	mov	r3, r2
    8180:	e1a03083 	lsl	r3, r3, #1
    8184:	e0833002 	add	r3, r3, r2
    8188:	e51b200c 	ldr	r2, [fp, #-12]
    818c:	e0833002 	add	r3, r3, r2
    8190:	e1a03183 	lsl	r3, r3, #3
    8194:	e0811003 	add	r1, r1, r3
    8198:	e24b301c 	sub	r3, fp, #28
    819c:	e893000c 	ldm	r3, {r2, r3}
    81a0:	e881000c 	stm	r1, {r2, r3}
    81a4:	e51b300c 	ldr	r3, [fp, #-12]
    81a8:	e2833001 	add	r3, r3, #1
    81ac:	e50b300c 	str	r3, [fp, #-12]
    81b0:	e51b200c 	ldr	r2, [fp, #-12]
    81b4:	e51b3024 	ldr	r3, [fp, #-36]	; 0xffffffdc
    81b8:	e1520003 	cmp	r2, r3
    81bc:	baffffc6 	blt	80dc <minver_mmul+0x80>
    81c0:	e51b3008 	ldr	r3, [fp, #-8]
    81c4:	e2833001 	add	r3, r3, #1
    81c8:	e50b3008 	str	r3, [fp, #-8]
    81cc:	e51b2008 	ldr	r2, [fp, #-8]
    81d0:	e51b3020 	ldr	r3, [fp, #-32]	; 0xffffffe0
    81d4:	e1520003 	cmp	r2, r3
    81d8:	baffffbc 	blt	80d0 <minver_mmul+0x74>
    81dc:	e3a03000 	mov	r3, #0
    81e0:	e1a00003 	mov	r0, r3
    81e4:	e28bd000 	add	sp, fp, #0
    81e8:	e49db004 	pop	{fp}		; (ldr fp, [sp], #4)
    81ec:	e12fff1e 	bx	lr
    81f0:	000003e7 	andeq	r0, r0, r7, ror #7
    81f4:	00018c50 	andeq	r8, r1, r0, asr ip
    81f8:	00018c98 	muleq	r1, r8, ip
    81fc:	00018ce0 	andeq	r8, r1, r0, ror #25

00008200 <minver_minver>:
    8200:	e92d4800 	push	{fp, lr}
    8204:	e28db004 	add	fp, sp, #4
    8208:	e24dde82 	sub	sp, sp, #2080	; 0x820
    820c:	e50b0818 	str	r0, [fp, #-2072]	; 0xfffff7e8
    8210:	e24b3e82 	sub	r3, fp, #2080	; 0x820
    8214:	e2433004 	sub	r3, r3, #4
    8218:	ed830b00 	vstr	d0, [r3]
    821c:	e3a03000 	mov	r3, #0
    8220:	e50b3014 	str	r3, [fp, #-20]	; 0xffffffec
    8224:	e3a02000 	mov	r2, #0
    8228:	e3a03000 	mov	r3, #0
    822c:	e50b201c 	str	r2, [fp, #-28]	; 0xffffffe4
    8230:	e50b3018 	str	r3, [fp, #-24]	; 0xffffffe8
    8234:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    8238:	e3530001 	cmp	r3, #1
    823c:	da000008 	ble	8264 <minver_minver+0x64>
    8240:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    8244:	e3530f7d 	cmp	r3, #500	; 0x1f4
    8248:	ca000005 	bgt	8264 <minver_minver+0x64>
    824c:	e24b3e82 	sub	r3, fp, #2080	; 0x820
    8250:	e2433004 	sub	r3, r3, #4
    8254:	edd30b00 	vldr	d16, [r3]
    8258:	eef50bc0 	vcmpe.f64	d16, #0.0
    825c:	eef1fa10 	vmrs	APSR_nzcv, fpscr
    8260:	8a000001 	bhi	826c <minver_minver+0x6c>
    8264:	e59f366c 	ldr	r3, [pc, #1644]	; 88d8 <minver_minver+0x6d8>
    8268:	ea000196 	b	88c8 <minver_minver+0x6c8>
    826c:	e3a02000 	mov	r2, #0
    8270:	e59f3664 	ldr	r3, [pc, #1636]	; 88dc <minver_minver+0x6dc>
    8274:	e50b202c 	str	r2, [fp, #-44]	; 0xffffffd4
    8278:	e50b3028 	str	r3, [fp, #-40]	; 0xffffffd8
    827c:	e3a03000 	mov	r3, #0
    8280:	e50b3008 	str	r3, [fp, #-8]
    8284:	ea000008 	b	82ac <minver_minver+0xac>
    8288:	e51b3008 	ldr	r3, [fp, #-8]
    828c:	e1a03103 	lsl	r3, r3, #2
    8290:	e2433004 	sub	r3, r3, #4
    8294:	e083300b 	add	r3, r3, fp
    8298:	e51b2008 	ldr	r2, [fp, #-8]
    829c:	e5032810 	str	r2, [r3, #-2064]	; 0xfffff7f0
    82a0:	e51b3008 	ldr	r3, [fp, #-8]
    82a4:	e2833001 	add	r3, r3, #1
    82a8:	e50b3008 	str	r3, [fp, #-8]
    82ac:	e51b2008 	ldr	r2, [fp, #-8]
    82b0:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    82b4:	e1520003 	cmp	r2, r3
    82b8:	bafffff2 	blt	8288 <minver_minver+0x88>
    82bc:	e3a03000 	mov	r3, #0
    82c0:	e50b3010 	str	r3, [fp, #-16]
    82c4:	ea00011e 	b	8744 <minver_minver+0x544>
    82c8:	e3a02000 	mov	r2, #0
    82cc:	e3a03000 	mov	r3, #0
    82d0:	e50b2024 	str	r2, [fp, #-36]	; 0xffffffdc
    82d4:	e50b3020 	str	r3, [fp, #-32]	; 0xffffffe0
    82d8:	e51b3010 	ldr	r3, [fp, #-16]
    82dc:	e50b3008 	str	r3, [fp, #-8]
    82e0:	ea00001a 	b	8350 <minver_minver+0x150>
    82e4:	e59f15f4 	ldr	r1, [pc, #1524]	; 88e0 <minver_minver+0x6e0>
    82e8:	e51b2008 	ldr	r2, [fp, #-8]
    82ec:	e1a03002 	mov	r3, r2
    82f0:	e1a03083 	lsl	r3, r3, #1
    82f4:	e0833002 	add	r3, r3, r2
    82f8:	e51b2010 	ldr	r2, [fp, #-16]
    82fc:	e0833002 	add	r3, r3, r2
    8300:	e1a03183 	lsl	r3, r3, #3
    8304:	e0813003 	add	r3, r1, r3
    8308:	edd30b00 	vldr	d16, [r3]
    830c:	eeb00b60 	vmov.f64	d0, d16
    8310:	ebffff3a 	bl	8000 <minver_fabs>
    8314:	ed0b0b07 	vstr	d0, [fp, #-28]	; 0xffffffe4
    8318:	ed5b1b07 	vldr	d17, [fp, #-28]	; 0xffffffe4
    831c:	ed5b0b09 	vldr	d16, [fp, #-36]	; 0xffffffdc
    8320:	eef41be0 	vcmpe.f64	d17, d16
    8324:	eef1fa10 	vmrs	APSR_nzcv, fpscr
    8328:	da000005 	ble	8344 <minver_minver+0x144>
    832c:	e24b301c 	sub	r3, fp, #28
    8330:	e893000c 	ldm	r3, {r2, r3}
    8334:	e50b2024 	str	r2, [fp, #-36]	; 0xffffffdc
    8338:	e50b3020 	str	r3, [fp, #-32]	; 0xffffffe0
    833c:	e51b3008 	ldr	r3, [fp, #-8]
    8340:	e50b3014 	str	r3, [fp, #-20]	; 0xffffffec
    8344:	e51b3008 	ldr	r3, [fp, #-8]
    8348:	e2833001 	add	r3, r3, #1
    834c:	e50b3008 	str	r3, [fp, #-8]
    8350:	e51b2008 	ldr	r2, [fp, #-8]
    8354:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    8358:	e1520003 	cmp	r2, r3
    835c:	baffffe0 	blt	82e4 <minver_minver+0xe4>
    8360:	e59f1578 	ldr	r1, [pc, #1400]	; 88e0 <minver_minver+0x6e0>
    8364:	e51b2014 	ldr	r2, [fp, #-20]	; 0xffffffec
    8368:	e1a03002 	mov	r3, r2
    836c:	e1a03083 	lsl	r3, r3, #1
    8370:	e0833002 	add	r3, r3, r2
    8374:	e51b2010 	ldr	r2, [fp, #-16]
    8378:	e0833002 	add	r3, r3, r2
    837c:	e1a03183 	lsl	r3, r3, #3
    8380:	e0813003 	add	r3, r1, r3
    8384:	e893000c 	ldm	r3, {r2, r3}
    8388:	e50b203c 	str	r2, [fp, #-60]	; 0xffffffc4
    838c:	e50b3038 	str	r3, [fp, #-56]	; 0xffffffc8
    8390:	ed1b0b0f 	vldr	d0, [fp, #-60]	; 0xffffffc4
    8394:	ebffff19 	bl	8000 <minver_fabs>
    8398:	ed0b0b11 	vstr	d0, [fp, #-68]	; 0xffffffbc
    839c:	e24b3e82 	sub	r3, fp, #2080	; 0x820
    83a0:	e2433004 	sub	r3, r3, #4
    83a4:	ed5b1b11 	vldr	d17, [fp, #-68]	; 0xffffffbc
    83a8:	edd30b00 	vldr	d16, [r3]
    83ac:	eef41be0 	vcmpe.f64	d17, d16
    83b0:	eef1fa10 	vmrs	APSR_nzcv, fpscr
    83b4:	8a000005 	bhi	83d0 <minver_minver+0x1d0>
    83b8:	e59f1524 	ldr	r1, [pc, #1316]	; 88e4 <minver_minver+0x6e4>
    83bc:	e24b302c 	sub	r3, fp, #44	; 0x2c
    83c0:	e893000c 	ldm	r3, {r2, r3}
    83c4:	e881000c 	stm	r1, {r2, r3}
    83c8:	e3a03001 	mov	r3, #1
    83cc:	ea00013d 	b	88c8 <minver_minver+0x6c8>
    83d0:	ed5b1b0b 	vldr	d17, [fp, #-44]	; 0xffffffd4
    83d4:	ed5b0b0f 	vldr	d16, [fp, #-60]	; 0xffffffc4
    83d8:	ee610ba0 	vmul.f64	d16, d17, d16
    83dc:	ed4b0b0b 	vstr	d16, [fp, #-44]	; 0xffffffd4
    83e0:	e51b2014 	ldr	r2, [fp, #-20]	; 0xffffffec
    83e4:	e51b3010 	ldr	r3, [fp, #-16]
    83e8:	e1520003 	cmp	r2, r3
    83ec:	0a00004e 	beq	852c <minver_minver+0x32c>
    83f0:	ed5b0b07 	vldr	d16, [fp, #-28]	; 0xffffffe4
    83f4:	eef10b60 	vneg.f64	d16, d16
    83f8:	ed4b0b0b 	vstr	d16, [fp, #-44]	; 0xffffffd4
    83fc:	e51b3010 	ldr	r3, [fp, #-16]
    8400:	e1a03103 	lsl	r3, r3, #2
    8404:	e2433004 	sub	r3, r3, #4
    8408:	e083300b 	add	r3, r3, fp
    840c:	e5133810 	ldr	r3, [r3, #-2064]	; 0xfffff7f0
    8410:	e50b3030 	str	r3, [fp, #-48]	; 0xffffffd0
    8414:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    8418:	e1a03103 	lsl	r3, r3, #2
    841c:	e2433004 	sub	r3, r3, #4
    8420:	e083300b 	add	r3, r3, fp
    8424:	e5132810 	ldr	r2, [r3, #-2064]	; 0xfffff7f0
    8428:	e51b3010 	ldr	r3, [fp, #-16]
    842c:	e1a03103 	lsl	r3, r3, #2
    8430:	e2433004 	sub	r3, r3, #4
    8434:	e083300b 	add	r3, r3, fp
    8438:	e5032810 	str	r2, [r3, #-2064]	; 0xfffff7f0
    843c:	e51b3014 	ldr	r3, [fp, #-20]	; 0xffffffec
    8440:	e1a03103 	lsl	r3, r3, #2
    8444:	e2433004 	sub	r3, r3, #4
    8448:	e083300b 	add	r3, r3, fp
    844c:	e51b2030 	ldr	r2, [fp, #-48]	; 0xffffffd0
    8450:	e5032810 	str	r2, [r3, #-2064]	; 0xfffff7f0
    8454:	e3a03000 	mov	r3, #0
    8458:	e50b300c 	str	r3, [fp, #-12]
    845c:	ea00002e 	b	851c <minver_minver+0x31c>
    8460:	e59f1478 	ldr	r1, [pc, #1144]	; 88e0 <minver_minver+0x6e0>
    8464:	e51b2010 	ldr	r2, [fp, #-16]
    8468:	e1a03002 	mov	r3, r2
    846c:	e1a03083 	lsl	r3, r3, #1
    8470:	e0833002 	add	r3, r3, r2
    8474:	e51b200c 	ldr	r2, [fp, #-12]
    8478:	e0833002 	add	r3, r3, r2
    847c:	e1a03183 	lsl	r3, r3, #3
    8480:	e0813003 	add	r3, r1, r3
    8484:	e893000c 	ldm	r3, {r2, r3}
    8488:	e50b201c 	str	r2, [fp, #-28]	; 0xffffffe4
    848c:	e50b3018 	str	r3, [fp, #-24]	; 0xffffffe8
    8490:	e59f1448 	ldr	r1, [pc, #1096]	; 88e0 <minver_minver+0x6e0>
    8494:	e51b2014 	ldr	r2, [fp, #-20]	; 0xffffffec
    8498:	e1a03002 	mov	r3, r2
    849c:	e1a03083 	lsl	r3, r3, #1
    84a0:	e0833002 	add	r3, r3, r2
    84a4:	e51b200c 	ldr	r2, [fp, #-12]
    84a8:	e0833002 	add	r3, r3, r2
    84ac:	e1a03183 	lsl	r3, r3, #3
    84b0:	e0813003 	add	r3, r1, r3
    84b4:	e8930003 	ldm	r3, {r0, r1}
    84b8:	e59fc420 	ldr	ip, [pc, #1056]	; 88e0 <minver_minver+0x6e0>
    84bc:	e51b2010 	ldr	r2, [fp, #-16]
    84c0:	e1a03002 	mov	r3, r2
    84c4:	e1a03083 	lsl	r3, r3, #1
    84c8:	e0833002 	add	r3, r3, r2
    84cc:	e51b200c 	ldr	r2, [fp, #-12]
    84d0:	e0833002 	add	r3, r3, r2
    84d4:	e1a03183 	lsl	r3, r3, #3
    84d8:	e08c3003 	add	r3, ip, r3
    84dc:	e8830003 	stm	r3, {r0, r1}
    84e0:	e59f13f8 	ldr	r1, [pc, #1016]	; 88e0 <minver_minver+0x6e0>
    84e4:	e51b2014 	ldr	r2, [fp, #-20]	; 0xffffffec
    84e8:	e1a03002 	mov	r3, r2
    84ec:	e1a03083 	lsl	r3, r3, #1
    84f0:	e0833002 	add	r3, r3, r2
    84f4:	e51b200c 	ldr	r2, [fp, #-12]
    84f8:	e0833002 	add	r3, r3, r2
    84fc:	e1a03183 	lsl	r3, r3, #3
    8500:	e0811003 	add	r1, r1, r3
    8504:	e24b301c 	sub	r3, fp, #28
    8508:	e893000c 	ldm	r3, {r2, r3}
    850c:	e881000c 	stm	r1, {r2, r3}
    8510:	e51b300c 	ldr	r3, [fp, #-12]
    8514:	e2833001 	add	r3, r3, #1
    8518:	e50b300c 	str	r3, [fp, #-12]
    851c:	e51b200c 	ldr	r2, [fp, #-12]
    8520:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    8524:	e1520003 	cmp	r2, r3
    8528:	baffffcc 	blt	8460 <minver_minver+0x260>
    852c:	e3a03000 	mov	r3, #0
    8530:	e50b3008 	str	r3, [fp, #-8]
    8534:	ea000018 	b	859c <minver_minver+0x39c>
    8538:	e59f13a0 	ldr	r1, [pc, #928]	; 88e0 <minver_minver+0x6e0>
    853c:	e51b2010 	ldr	r2, [fp, #-16]
    8540:	e1a03002 	mov	r3, r2
    8544:	e1a03083 	lsl	r3, r3, #1
    8548:	e0833002 	add	r3, r3, r2
    854c:	e51b2008 	ldr	r2, [fp, #-8]
    8550:	e0833002 	add	r3, r3, r2
    8554:	e1a03183 	lsl	r3, r3, #3
    8558:	e0813003 	add	r3, r1, r3
    855c:	edd32b00 	vldr	d18, [r3]
    8560:	ed5b1b0f 	vldr	d17, [fp, #-60]	; 0xffffffc4
    8564:	eec20ba1 	vdiv.f64	d16, d18, d17
    8568:	e59f1370 	ldr	r1, [pc, #880]	; 88e0 <minver_minver+0x6e0>
    856c:	e51b2010 	ldr	r2, [fp, #-16]
    8570:	e1a03002 	mov	r3, r2
    8574:	e1a03083 	lsl	r3, r3, #1
    8578:	e0833002 	add	r3, r3, r2
    857c:	e51b2008 	ldr	r2, [fp, #-8]
    8580:	e0833002 	add	r3, r3, r2
    8584:	e1a03183 	lsl	r3, r3, #3
    8588:	e0813003 	add	r3, r1, r3
    858c:	edc30b00 	vstr	d16, [r3]
    8590:	e51b3008 	ldr	r3, [fp, #-8]
    8594:	e2833001 	add	r3, r3, #1
    8598:	e50b3008 	str	r3, [fp, #-8]
    859c:	e51b2008 	ldr	r2, [fp, #-8]
    85a0:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    85a4:	e1520003 	cmp	r2, r3
    85a8:	baffffe2 	blt	8538 <minver_minver+0x338>
    85ac:	e3a03000 	mov	r3, #0
    85b0:	e50b3008 	str	r3, [fp, #-8]
    85b4:	ea000053 	b	8708 <minver_minver+0x508>
    85b8:	e51b2008 	ldr	r2, [fp, #-8]
    85bc:	e51b3010 	ldr	r3, [fp, #-16]
    85c0:	e1520003 	cmp	r2, r3
    85c4:	0a00004c 	beq	86fc <minver_minver+0x4fc>
    85c8:	e59f1310 	ldr	r1, [pc, #784]	; 88e0 <minver_minver+0x6e0>
    85cc:	e51b2008 	ldr	r2, [fp, #-8]
    85d0:	e1a03002 	mov	r3, r2
    85d4:	e1a03083 	lsl	r3, r3, #1
    85d8:	e0833002 	add	r3, r3, r2
    85dc:	e51b2010 	ldr	r2, [fp, #-16]
    85e0:	e0833002 	add	r3, r3, r2
    85e4:	e1a03183 	lsl	r3, r3, #3
    85e8:	e0813003 	add	r3, r1, r3
    85ec:	e893000c 	ldm	r3, {r2, r3}
    85f0:	e50b201c 	str	r2, [fp, #-28]	; 0xffffffe4
    85f4:	e50b3018 	str	r3, [fp, #-24]	; 0xffffffe8
    85f8:	ed5b0b07 	vldr	d16, [fp, #-28]	; 0xffffffe4
    85fc:	eef50b40 	vcmp.f64	d16, #0.0
    8600:	eef1fa10 	vmrs	APSR_nzcv, fpscr
    8604:	0a00003c 	beq	86fc <minver_minver+0x4fc>
    8608:	e3a03000 	mov	r3, #0
    860c:	e50b300c 	str	r3, [fp, #-12]
    8610:	ea000027 	b	86b4 <minver_minver+0x4b4>
    8614:	e51b200c 	ldr	r2, [fp, #-12]
    8618:	e51b3010 	ldr	r3, [fp, #-16]
    861c:	e1520003 	cmp	r2, r3
    8620:	0a000020 	beq	86a8 <minver_minver+0x4a8>
    8624:	e59f12b4 	ldr	r1, [pc, #692]	; 88e0 <minver_minver+0x6e0>
    8628:	e51b2008 	ldr	r2, [fp, #-8]
    862c:	e1a03002 	mov	r3, r2
    8630:	e1a03083 	lsl	r3, r3, #1
    8634:	e0833002 	add	r3, r3, r2
    8638:	e51b200c 	ldr	r2, [fp, #-12]
    863c:	e0833002 	add	r3, r3, r2
    8640:	e1a03183 	lsl	r3, r3, #3
    8644:	e0813003 	add	r3, r1, r3
    8648:	edd31b00 	vldr	d17, [r3]
    864c:	e59f128c 	ldr	r1, [pc, #652]	; 88e0 <minver_minver+0x6e0>
    8650:	e51b2010 	ldr	r2, [fp, #-16]
    8654:	e1a03002 	mov	r3, r2
    8658:	e1a03083 	lsl	r3, r3, #1
    865c:	e0833002 	add	r3, r3, r2
    8660:	e51b200c 	ldr	r2, [fp, #-12]
    8664:	e0833002 	add	r3, r3, r2
    8668:	e1a03183 	lsl	r3, r3, #3
    866c:	e0813003 	add	r3, r1, r3
    8670:	edd32b00 	vldr	d18, [r3]
    8674:	ed5b0b07 	vldr	d16, [fp, #-28]	; 0xffffffe4
    8678:	ee620ba0 	vmul.f64	d16, d18, d16
    867c:	ee710be0 	vsub.f64	d16, d17, d16
    8680:	e59f1258 	ldr	r1, [pc, #600]	; 88e0 <minver_minver+0x6e0>
    8684:	e51b2008 	ldr	r2, [fp, #-8]
    8688:	e1a03002 	mov	r3, r2
    868c:	e1a03083 	lsl	r3, r3, #1
    8690:	e0833002 	add	r3, r3, r2
    8694:	e51b200c 	ldr	r2, [fp, #-12]
    8698:	e0833002 	add	r3, r3, r2
    869c:	e1a03183 	lsl	r3, r3, #3
    86a0:	e0813003 	add	r3, r1, r3
    86a4:	edc30b00 	vstr	d16, [r3]
    86a8:	e51b300c 	ldr	r3, [fp, #-12]
    86ac:	e2833001 	add	r3, r3, #1
    86b0:	e50b300c 	str	r3, [fp, #-12]
    86b4:	e51b200c 	ldr	r2, [fp, #-12]
    86b8:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    86bc:	e1520003 	cmp	r2, r3
    86c0:	baffffd3 	blt	8614 <minver_minver+0x414>
    86c4:	ed5b0b07 	vldr	d16, [fp, #-28]	; 0xffffffe4
    86c8:	eef12b60 	vneg.f64	d18, d16
    86cc:	ed5b1b0f 	vldr	d17, [fp, #-60]	; 0xffffffc4
    86d0:	eec20ba1 	vdiv.f64	d16, d18, d17
    86d4:	e59f1204 	ldr	r1, [pc, #516]	; 88e0 <minver_minver+0x6e0>
    86d8:	e51b2008 	ldr	r2, [fp, #-8]
    86dc:	e1a03002 	mov	r3, r2
    86e0:	e1a03083 	lsl	r3, r3, #1
    86e4:	e0833002 	add	r3, r3, r2
    86e8:	e51b2010 	ldr	r2, [fp, #-16]
    86ec:	e0833002 	add	r3, r3, r2
    86f0:	e1a03183 	lsl	r3, r3, #3
    86f4:	e0813003 	add	r3, r1, r3
    86f8:	edc30b00 	vstr	d16, [r3]
    86fc:	e51b3008 	ldr	r3, [fp, #-8]
    8700:	e2833001 	add	r3, r3, #1
    8704:	e50b3008 	str	r3, [fp, #-8]
    8708:	e51b2008 	ldr	r2, [fp, #-8]
    870c:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    8710:	e1520003 	cmp	r2, r3
    8714:	baffffa7 	blt	85b8 <minver_minver+0x3b8>
    8718:	eef72b00 	vmov.f64	d18, #112	; 0x3f800000  1.0
    871c:	ed5b1b0f 	vldr	d17, [fp, #-60]	; 0xffffffc4
    8720:	eec20ba1 	vdiv.f64	d16, d18, d17
    8724:	e59f21b4 	ldr	r2, [pc, #436]	; 88e0 <minver_minver+0x6e0>
    8728:	e51b3010 	ldr	r3, [fp, #-16]
    872c:	e1a03283 	lsl	r3, r3, #5
    8730:	e0823003 	add	r3, r2, r3
    8734:	edc30b00 	vstr	d16, [r3]
    8738:	e51b3010 	ldr	r3, [fp, #-16]
    873c:	e2833001 	add	r3, r3, #1
    8740:	e50b3010 	str	r3, [fp, #-16]
    8744:	e51b2010 	ldr	r2, [fp, #-16]
    8748:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    874c:	e1520003 	cmp	r2, r3
    8750:	bafffedc 	blt	82c8 <minver_minver+0xc8>
    8754:	e3a03000 	mov	r3, #0
    8758:	e50b3008 	str	r3, [fp, #-8]
    875c:	ea000050 	b	88a4 <minver_minver+0x6a4>
    8760:	e51b3008 	ldr	r3, [fp, #-8]
    8764:	e1a03103 	lsl	r3, r3, #2
    8768:	e2433004 	sub	r3, r3, #4
    876c:	e083300b 	add	r3, r3, fp
    8770:	e5133810 	ldr	r3, [r3, #-2064]	; 0xfffff7f0
    8774:	e50b3010 	str	r3, [fp, #-16]
    8778:	e51b2010 	ldr	r2, [fp, #-16]
    877c:	e51b3008 	ldr	r3, [fp, #-8]
    8780:	e1520003 	cmp	r2, r3
    8784:	0a000042 	beq	8894 <minver_minver+0x694>
    8788:	e51b3010 	ldr	r3, [fp, #-16]
    878c:	e1a03103 	lsl	r3, r3, #2
    8790:	e2433004 	sub	r3, r3, #4
    8794:	e083300b 	add	r3, r3, fp
    8798:	e5133810 	ldr	r3, [r3, #-2064]	; 0xfffff7f0
    879c:	e50b3030 	str	r3, [fp, #-48]	; 0xffffffd0
    87a0:	e51b3008 	ldr	r3, [fp, #-8]
    87a4:	e1a03103 	lsl	r3, r3, #2
    87a8:	e2433004 	sub	r3, r3, #4
    87ac:	e083300b 	add	r3, r3, fp
    87b0:	e5132810 	ldr	r2, [r3, #-2064]	; 0xfffff7f0
    87b4:	e51b3010 	ldr	r3, [fp, #-16]
    87b8:	e1a03103 	lsl	r3, r3, #2
    87bc:	e2433004 	sub	r3, r3, #4
    87c0:	e083300b 	add	r3, r3, fp
    87c4:	e5032810 	str	r2, [r3, #-2064]	; 0xfffff7f0
    87c8:	e51b3008 	ldr	r3, [fp, #-8]
    87cc:	e1a03103 	lsl	r3, r3, #2
    87d0:	e2433004 	sub	r3, r3, #4
    87d4:	e083300b 	add	r3, r3, fp
    87d8:	e51b2030 	ldr	r2, [fp, #-48]	; 0xffffffd0
    87dc:	e5032810 	str	r2, [r3, #-2064]	; 0xfffff7f0
    87e0:	e3a03000 	mov	r3, #0
    87e4:	e50b300c 	str	r3, [fp, #-12]
    87e8:	ea000024 	b	8880 <minver_minver+0x680>
    87ec:	e59f10ec 	ldr	r1, [pc, #236]	; 88e0 <minver_minver+0x6e0>
    87f0:	e51b2010 	ldr	r2, [fp, #-16]
    87f4:	e1a03002 	mov	r3, r2
    87f8:	e1a03083 	lsl	r3, r3, #1
    87fc:	e0833002 	add	r3, r3, r2
    8800:	e51b2008 	ldr	r2, [fp, #-8]
    8804:	e0833002 	add	r3, r3, r2
    8808:	e1a03183 	lsl	r3, r3, #3
    880c:	e0813003 	add	r3, r1, r3
    8810:	e893000c 	ldm	r3, {r2, r3}
    8814:	e50b201c 	str	r2, [fp, #-28]	; 0xffffffe4
    8818:	e50b3018 	str	r3, [fp, #-24]	; 0xffffffe8
    881c:	e59f20bc 	ldr	r2, [pc, #188]	; 88e0 <minver_minver+0x6e0>
    8820:	e51b3010 	ldr	r3, [fp, #-16]
    8824:	e1a03283 	lsl	r3, r3, #5
    8828:	e0823003 	add	r3, r2, r3
    882c:	e8930003 	ldm	r3, {r0, r1}
    8830:	e59fc0a8 	ldr	ip, [pc, #168]	; 88e0 <minver_minver+0x6e0>
    8834:	e51b2010 	ldr	r2, [fp, #-16]
    8838:	e1a03002 	mov	r3, r2
    883c:	e1a03083 	lsl	r3, r3, #1
    8840:	e0833002 	add	r3, r3, r2
    8844:	e51b2008 	ldr	r2, [fp, #-8]
    8848:	e0833002 	add	r3, r3, r2
    884c:	e1a03183 	lsl	r3, r3, #3
    8850:	e08c3003 	add	r3, ip, r3
    8854:	e8830003 	stm	r3, {r0, r1}
    8858:	e59f2080 	ldr	r2, [pc, #128]	; 88e0 <minver_minver+0x6e0>
    885c:	e51b3010 	ldr	r3, [fp, #-16]
    8860:	e1a03283 	lsl	r3, r3, #5
    8864:	e0821003 	add	r1, r2, r3
    8868:	e24b301c 	sub	r3, fp, #28
    886c:	e893000c 	ldm	r3, {r2, r3}
    8870:	e881000c 	stm	r1, {r2, r3}
    8874:	e51b300c 	ldr	r3, [fp, #-12]
    8878:	e2833001 	add	r3, r3, #1
    887c:	e50b300c 	str	r3, [fp, #-12]
    8880:	e51b200c 	ldr	r2, [fp, #-12]
    8884:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    8888:	e1520003 	cmp	r2, r3
    888c:	baffffd6 	blt	87ec <minver_minver+0x5ec>
    8890:	eaffffb2 	b	8760 <minver_minver+0x560>
    8894:	e1a00000 	nop			; (mov r0, r0)
    8898:	e51b3008 	ldr	r3, [fp, #-8]
    889c:	e2833001 	add	r3, r3, #1
    88a0:	e50b3008 	str	r3, [fp, #-8]
    88a4:	e51b2008 	ldr	r2, [fp, #-8]
    88a8:	e51b3818 	ldr	r3, [fp, #-2072]	; 0xfffff7e8
    88ac:	e1520003 	cmp	r2, r3
    88b0:	baffffaa 	blt	8760 <minver_minver+0x560>
    88b4:	e59f1028 	ldr	r1, [pc, #40]	; 88e4 <minver_minver+0x6e4>
    88b8:	e24b302c 	sub	r3, fp, #44	; 0x2c
    88bc:	e893000c 	ldm	r3, {r2, r3}
    88c0:	e881000c 	stm	r1, {r2, r3}
    88c4:	e3a03000 	mov	r3, #0
    88c8:	e1a00003 	mov	r0, r3
    88cc:	e24bd004 	sub	sp, fp, #4
    88d0:	e8bd4800 	pop	{fp, lr}
    88d4:	e12fff1e 	bx	lr
    88d8:	000003e7 	andeq	r0, r0, r7, ror #7
    88dc:	3ff00000 	svccc	0x00f00000	; IMB
    88e0:	00018c50 	andeq	r8, r1, r0, asr ip
    88e4:	00018db8 			; <UNDEFINED> instruction: 0x00018db8

Disassembly of section .data:

00018c50 <minver_a>:
   18c50:	00000000 	andeq	r0, r0, r0
   18c54:	40080000 	andmi	r0, r8, r0
   18c58:	00000000 	andeq	r0, r0, r0
   18c5c:	c0180000 	andsgt	r0, r8, r0
   18c60:	00000000 	andeq	r0, r0, r0
   18c64:	401c0000 	andsmi	r0, ip, r0
   18c68:	00000000 	andeq	r0, r0, r0
   18c6c:	40220000 	eormi	r0, r2, r0
	...
   18c7c:	c0140000 	andsgt	r0, r4, r0
   18c80:	00000000 	andeq	r0, r0, r0
   18c84:	40140000 	andsmi	r0, r4, r0
   18c88:	00000000 	andeq	r0, r0, r0
   18c8c:	c0200000 	eorgt	r0, r0, r0
   18c90:	00000000 	andeq	r0, r0, r0
   18c94:	40180000 	andsmi	r0, r8, r0

Disassembly of section .bss:

00018c98 <minver_b>:
	...

00018ce0 <minver_c>:
	...

00018d28 <minver_aa>:
	...

00018d70 <minver_a_i>:
	...

00018db8 <minver_det>:
	...

Disassembly of section .comment:

00000000 <.comment>:
   0:	3a434347 	bcc	10d0d24 <_stack+0x1050d24>
   4:	35312820 	ldrcc	r2, [r1, #-2080]!	; 0xfffff7e0
   8:	2e30313a 	mrccs	1, 1, r3, cr0, cr10, {1}
   c:	30322d33 	eorscc	r2, r2, r3, lsr sp
  10:	302e3132 	eorcc	r3, lr, r2, lsr r1
  14:	29342d37 	ldmdbcs	r4!, {r0, r1, r2, r4, r5, r8, sl, fp, sp}
  18:	2e303120 	rsfcssp	f3, f0, f0
  1c:	20312e33 	eorscs	r2, r1, r3, lsr lr
  20:	31323032 	teqcc	r2, r2, lsr r0
  24:	31323630 	teqcc	r2, r0, lsr r6
  28:	65722820 	ldrbvs	r2, [r2, #-2080]!	; 0xfffff7e0
  2c:	7361656c 	cmnvc	r1, #108, 10	; 0x1b000000
  30:	Address 0x0000000000000030 is out of bounds.


Disassembly of section .debug_aranges:

00000000 <.debug_aranges>:
   0:	0000001c 	andeq	r0, r0, ip, lsl r0
   4:	00000002 	andeq	r0, r0, r2
   8:	00040000 	andeq	r0, r4, r0
   c:	00000000 	andeq	r0, r0, r0
  10:	00008000 	andeq	r8, r0, r0
  14:	00000c4c 	andeq	r0, r0, ip, asr #24
	...

Disassembly of section .debug_info:

00000000 <.debug_info>:
   0:	00000357 	andeq	r0, r0, r7, asr r3
   4:	00000004 	andeq	r0, r0, r4
   8:	01040000 	mrseq	r0, (UNDEF: 4)
   c:	00000000 	andeq	r0, r0, r0
  10:	0000e80c 	andeq	lr, r0, ip, lsl #16
  14:	0000fd00 	andeq	pc, r0, r0, lsl #26
  18:	00800000 	addeq	r0, r0, r0
  1c:	000c4c00 	andeq	r4, ip, r0, lsl #24
  20:	00000000 	andeq	r0, r0, r0
  24:	00420200 	subeq	r0, r2, r0, lsl #4
  28:	003b0000 	eorseq	r0, fp, r0
  2c:	3b030000 	blcc	c0034 <_stack+0x40034>
  30:	02000000 	andeq	r0, r0, #0
  34:	00003b03 	andeq	r3, r0, r3, lsl #22
  38:	04000200 	streq	r0, [r0], #-512	; 0xfffffe00
  3c:	017c0704 	cmneq	ip, r4, lsl #14
  40:	08040000 	stmdaeq	r4, {}	; <UNPREDICTABLE>
  44:	0000f604 	andeq	pc, r0, r4, lsl #12
  48:	009a0500 	addseq	r0, sl, r0, lsl #10
  4c:	2d010000 	stccs	0, cr0, [r1, #-0]
  50:	00002509 	andeq	r2, r0, r9, lsl #10
  54:	50030500 	andpl	r0, r3, r0, lsl #10
  58:	0500018c 	streq	r0, [r0, #-396]	; 0xfffffe74
  5c:	000000a3 	andeq	r0, r0, r3, lsr #1
  60:	25083201 	strcs	r3, [r8, #-513]	; 0xfffffdff
  64:	05000000 	streq	r0, [r0, #-0]
  68:	018c9803 	orreq	r9, ip, r3, lsl #16
  6c:	00ac0500 	adceq	r0, ip, r0, lsl #10
  70:	33010000 	movwcc	r0, #4096	; 0x1000
  74:	00002508 	andeq	r2, r0, r8, lsl #10
  78:	e0030500 	and	r0, r3, r0, lsl #10
  7c:	0500018c 	streq	r0, [r0, #-396]	; 0xfffffe74
  80:	00000090 	muleq	r0, r0, r0
  84:	25083401 	strcs	r3, [r8, #-1025]	; 0xfffffbff
  88:	05000000 	streq	r0, [r0, #-0]
  8c:	018d2803 	orreq	r2, sp, r3, lsl #16
  90:	01710500 	cmneq	r1, r0, lsl #10
  94:	35010000 	strcc	r0, [r1, #-0]
  98:	00002508 	andeq	r2, r0, r8, lsl #10
  9c:	70030500 	andvc	r0, r3, r0, lsl #10
  a0:	0500018d 	streq	r0, [r0, #-397]	; 0xfffffe73
  a4:	000000b5 	strheq	r0, [r0], -r5
  a8:	42083601 	andmi	r3, r8, #1048576	; 0x100000
  ac:	05000000 	streq	r0, [r0, #-0]
  b0:	018db803 	orreq	fp, sp, r3, lsl #16
  b4:	008b0600 	addeq	r0, fp, r0, lsl #12
  b8:	fa010000 	blx	400c0 <__bss_end__+0x27300>
  bc:	0000cb05 	andeq	ip, r0, r5, lsl #22
  c0:	008c2400 	addeq	r2, ip, r0, lsl #8
  c4:	00002800 	andeq	r2, r0, r0, lsl #16
  c8:	079c0100 	ldreq	r0, [ip, r0, lsl #2]
  cc:	6e690504 	cdpvs	5, 6, cr0, cr9, cr4, {0}
  d0:	cb080074 	blgt	2002a8 <_stack+0x1802a8>
  d4:	09000000 	stmdbeq	r0, {}	; <UNPREDICTABLE>
  d8:	00000084 	andeq	r0, r0, r4, lsl #1
  dc:	941ee201 	ldrls	lr, [lr], #-513	; 0xfffffdff
  e0:	9000008a 	andls	r0, r0, sl, lsl #1
  e4:	01000001 	tsteq	r0, r1
  e8:	0001179c 	muleq	r1, ip, r7
  ec:	00690a00 	rsbeq	r0, r9, r0, lsl #20
  f0:	cb07e401 	blgt	1f90fc <_stack+0x1790fc>
  f4:	02000000 	andeq	r0, r0, #0
  f8:	6a0a7491 	bvs	29d344 <_stack+0x21d344>
  fc:	0ae40100 	beq	ff900504 <_stack+0xff880504>
 100:	000000cb 	andeq	r0, r0, fp, asr #1
 104:	0a709102 	beq	1c24514 <_stack+0x1ba4514>
 108:	00737065 	rsbseq	r7, r3, r5, rrx
 10c:	420ae501 	andmi	lr, sl, #4194304	; 0x400000
 110:	02000000 	andeq	r0, r0, #0
 114:	0b006891 	bleq	1a360 <__bss_end__+0x15a0>
 118:	0000018f 	andeq	r0, r0, pc, lsl #3
 11c:	cb05cd01 	blgt	173528 <_stack+0xf3528>
 120:	bc000000 	stclt	0, cr0, [r0], {-0}
 124:	d8000089 	stmdale	r0, {r0, r3, r7}
 128:	01000000 	mrseq	r0, (UNDEF: 0)
 12c:	00015b9c 	muleq	r1, ip, fp
 130:	00690a00 	rsbeq	r0, r9, r0, lsl #20
 134:	cb07cf01 	blgt	1f3d40 <_stack+0x173d40>
 138:	02000000 	andeq	r0, r0, #0
 13c:	6a0a7491 	bvs	29d388 <_stack+0x21d388>
 140:	0acf0100 	beq	ff3c0548 <_stack+0xff340548>
 144:	000000cb 	andeq	r0, r0, fp, asr #1
 148:	0c709102 	ldfeqp	f1, [r0], #-8
 14c:	000000de 	ldrdeq	r0, [r0], -lr
 150:	420ad001 	andmi	sp, sl, #1
 154:	02000000 	andeq	r0, r0, #0
 158:	0d006891 	stceq	8, cr6, [r0, #-580]	; 0xfffffdbc
 15c:	0000015f 	andeq	r0, r0, pc, asr r1
 160:	e806bf01 	stmda	r6, {r0, r8, r9, sl, fp, ip, sp, pc}
 164:	d4000088 	strle	r0, [r0], #-136	; 0xffffff78
 168:	01000000 	mrseq	r0, (UNDEF: 0)
 16c:	0001999c 	muleq	r1, ip, r9
 170:	00690a00 	rsbeq	r0, r9, r0, lsl #20
 174:	cb07c101 	blgt	1f0580 <_stack+0x170580>
 178:	02000000 	andeq	r0, r0, #0
 17c:	6a0a7491 	bvs	29d3c8 <_stack+0x21d3c8>
 180:	0ac10100 	beq	ff040588 <_stack+0xfefc0588>
 184:	000000cb 	andeq	r0, r0, fp, asr #1
 188:	0a709102 	beq	1c24598 <_stack+0x1ba4598>
 18c:	c2010078 	andgt	r0, r1, #120	; 0x78
 190:	0000d210 	andeq	sp, r0, r0, lsl r2
 194:	6c910200 	lfmvs	f0, 4, [r1], {0}
 198:	014c0e00 	cmpeq	ip, r0, lsl #28
 19c:	66010000 	strvs	r0, [r1], -r0
 1a0:	0000cb05 	andeq	ip, r0, r5, lsl #22
 1a4:	00820000 	addeq	r0, r2, r0
 1a8:	0006e800 	andeq	lr, r6, r0, lsl #16
 1ac:	6f9c0100 	svcvs	0x009c0100
 1b0:	0f000002 	svceq	0x00000002
 1b4:	000000f1 	strdeq	r0, [r0], -r1
 1b8:	cb186601 	blgt	6199c4 <_stack+0x5999c4>
 1bc:	03000000 	movweq	r0, #0
 1c0:	106fe491 	mlsne	pc, r1, r4, lr	; <UNPREDICTABLE>
 1c4:	00737065 	rsbseq	r7, r3, r5, rrx
 1c8:	42256601 	eormi	r6, r5, #1048576	; 0x100000
 1cc:	03000000 	movweq	r0, #0
 1d0:	0c6fd891 	stcleq	8, cr13, [pc], #-580	; ffffff94 <_stack+0xfff7ff94>
 1d4:	00000079 	andeq	r0, r0, r9, ror r0
 1d8:	6f076901 	svcvs	0x00076901
 1dc:	03000002 	movweq	r0, #2
 1e0:	0a6fe891 	beq	1bfa42c <_stack+0x1b7a42c>
 1e4:	69010069 	stmdbvs	r1, {r0, r3, r5, r6}
 1e8:	0000cb14 	andeq	ip, r0, r4, lsl fp
 1ec:	74910200 	ldrvc	r0, [r1], #512	; 0x200
 1f0:	01006a0a 	tsteq	r0, sl, lsl #20
 1f4:	00cb1769 	sbceq	r1, fp, r9, ror #14
 1f8:	91020000 	mrsls	r0, (UNDEF: 2)
 1fc:	006b0a70 	rsbeq	r0, fp, r0, ror sl
 200:	cb1a6901 	blgt	69a60c <_stack+0x61a60c>
 204:	02000000 	andeq	r0, r0, #0
 208:	690a6c91 	stmdbvs	sl, {r0, r4, r7, sl, fp, sp, lr}
 20c:	69010077 	stmdbvs	r1, {r0, r1, r2, r4, r5, r6}
 210:	0000cb1d 	andeq	ip, r0, sp, lsl fp
 214:	4c910200 	lfmmi	f0, 4, [r1], {0}
 218:	0100720a 	tsteq	r0, sl, lsl #4
 21c:	00cb076a 	sbceq	r0, fp, sl, ror #14
 220:	91020000 	mrsls	r0, (UNDEF: 2)
 224:	00770a68 	rsbseq	r0, r7, r8, ror #20
 228:	420a6b01 	andmi	r6, sl, #1024	; 0x400
 22c:	02000000 	andeq	r0, r0, #0
 230:	5a0c6091 	bpl	31847c <_stack+0x29847c>
 234:	01000001 	tsteq	r0, r1
 238:	0042116b 	subeq	r1, r2, fp, ror #2
 23c:	91020000 	mrsls	r0, (UNDEF: 2)
 240:	007e0c58 	rsbseq	r0, lr, r8, asr ip
 244:	6b010000 	blvs	4024c <__bss_end__+0x2748c>
 248:	00004217 	andeq	r4, r0, r7, lsl r2
 24c:	40910200 	addsmi	r0, r1, r0, lsl #4
 250:	6970610a 	ldmdbvs	r0!, {r1, r3, r8, sp, lr}^
 254:	1e6b0100 	pownee	f0, f3, f0
 258:	00000042 	andeq	r0, r0, r2, asr #32
 25c:	7fb89103 	svcvc	0x00b89103
 260:	0031770a 	eorseq	r7, r1, sl, lsl #14
 264:	42236b01 	eormi	r6, r3, #1024	; 0x400
 268:	02000000 	andeq	r0, r0, #0
 26c:	02005091 	andeq	r5, r0, #145	; 0x91
 270:	000000cb 	andeq	r0, r0, fp, asr #1
 274:	00000280 	andeq	r0, r0, r0, lsl #5
 278:	00003b11 	andeq	r3, r0, r1, lsl fp
 27c:	0001f300 	andeq	pc, r1, r0, lsl #6
 280:	0000c612 	andeq	ip, r0, r2, lsl r6
 284:	06490100 	strbeq	r0, [r9], -r0, lsl #2
 288:	000000cb 	andeq	r0, r0, fp, asr #1
 28c:	0000805c 	andeq	r8, r0, ip, asr r0
 290:	000001a4 	andeq	r0, r0, r4, lsr #3
 294:	03299c01 			; <UNDEFINED> instruction: 0x03299c01
 298:	3a0f0000 	bcc	3c02a0 <_stack+0x3402a0>
 29c:	01000001 	tsteq	r0, r1
 2a0:	00cb1749 	sbceq	r1, fp, r9, asr #14
 2a4:	91020000 	mrsls	r0, (UNDEF: 2)
 2a8:	016b0f54 	cmneq	fp, r4, asr pc
 2ac:	49010000 	stmdbmi	r1, {}	; <UNPREDICTABLE>
 2b0:	0000cb22 	andeq	ip, r0, r2, lsr #22
 2b4:	50910200 	addspl	r0, r1, r0, lsl #4
 2b8:	0001400f 	andeq	r4, r1, pc
 2bc:	2d490100 	stfcse	f0, [r9, #-0]
 2c0:	000000cb 	andeq	r0, r0, fp, asr #1
 2c4:	0f4c9102 	svceq	0x004c9102
 2c8:	000000c0 	andeq	r0, r0, r0, asr #1
 2cc:	cb384901 	blgt	e126d8 <_stack+0xd926d8>
 2d0:	02000000 	andeq	r0, r0, #0
 2d4:	690a4891 	stmdbvs	sl, {r0, r4, r7, fp, lr}
 2d8:	074b0100 	strbeq	r0, [fp, -r0, lsl #2]
 2dc:	000000cb 	andeq	r0, r0, fp, asr #1
 2e0:	0a749102 	beq	1d246f0 <_stack+0x1ca46f0>
 2e4:	4b01006a 	blmi	40494 <__bss_end__+0x276d4>
 2e8:	0000cb0a 	andeq	ip, r0, sl, lsl #22
 2ec:	70910200 	addsvc	r0, r1, r0, lsl #4
 2f0:	01006b0a 	tsteq	r0, sl, lsl #22
 2f4:	00cb0d4b 	sbceq	r0, fp, fp, asr #26
 2f8:	91020000 	mrsls	r0, (UNDEF: 2)
 2fc:	01460c6c 	cmpeq	r6, ip, ror #24
 300:	4b010000 	blmi	40308 <__bss_end__+0x27548>
 304:	0000cb10 	andeq	ip, r0, r0, lsl fp
 308:	5c910200 	lfmpl	f0, 4, [r1], {0}
 30c:	0001890c 	andeq	r8, r1, ip, lsl #18
 310:	174b0100 	strbne	r0, [fp, -r0, lsl #2]
 314:	000000cb 	andeq	r0, r0, fp, asr #1
 318:	0a589102 	beq	1624728 <_stack+0x15a4728>
 31c:	4c010077 	stcmi	0, cr0, [r1], {119}	; 0x77
 320:	0000420a 	andeq	r4, r0, sl, lsl #4
 324:	60910200 	addsvs	r0, r1, r0, lsl #4
 328:	00d21300 	sbcseq	r1, r2, r0, lsl #6
 32c:	3d010000 	stccc	0, cr0, [r1, #-0]
 330:	00004208 	andeq	r4, r0, r8, lsl #4
 334:	00800000 	addeq	r0, r0, r0
 338:	00005c00 	andeq	r5, r0, r0, lsl #24
 33c:	109c0100 	addsne	r0, ip, r0, lsl #2
 340:	3d01006e 	stccc	0, cr0, [r1, #-440]	; 0xfffffe48
 344:	0000421c 	andeq	r4, r0, ip, lsl r2
 348:	68910200 	ldmvs	r1, {r9}
 34c:	0100660a 	tsteq	r0, sl, lsl #12
 350:	00420a3f 	subeq	r0, r2, pc, lsr sl
 354:	91020000 	mrsls	r0, (UNDEF: 2)
 358:	Address 0x0000000000000358 is out of bounds.


Disassembly of section .debug_abbrev:

00000000 <.debug_abbrev>:
   0:	25011101 	strcs	r1, [r1, #-257]	; 0xfffffeff
   4:	030b130e 	movweq	r1, #45838	; 0xb30e
   8:	110e1b0e 	tstne	lr, lr, lsl #22
   c:	10061201 	andne	r1, r6, r1, lsl #4
  10:	02000017 	andeq	r0, r0, #23
  14:	13490101 	movtne	r0, #37121	; 0x9101
  18:	00001301 	andeq	r1, r0, r1, lsl #6
  1c:	49002103 	stmdbmi	r0, {r0, r1, r8, sp}
  20:	000b2f13 	andeq	r2, fp, r3, lsl pc
  24:	00240400 	eoreq	r0, r4, r0, lsl #8
  28:	0b3e0b0b 	bleq	f82c5c <_stack+0xf02c5c>
  2c:	00000e03 	andeq	r0, r0, r3, lsl #28
  30:	03003405 	movweq	r3, #1029	; 0x405
  34:	3b0b3a0e 	blcc	2ce874 <_stack+0x24e874>
  38:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
  3c:	02193f13 	andseq	r3, r9, #19, 30	; 0x4c
  40:	06000018 			; <UNDEFINED> instruction: 0x06000018
  44:	193f002e 	ldmdbne	pc!, {r1, r2, r3, r5}	; <UNPREDICTABLE>
  48:	0b3a0e03 	bleq	e8385c <_stack+0xe0385c>
  4c:	0b390b3b 	bleq	e42d40 <_stack+0xdc2d40>
  50:	13491927 	movtne	r1, #39207	; 0x9927
  54:	06120111 			; <UNDEFINED> instruction: 0x06120111
  58:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
  5c:	07000019 	smladeq	r0, r9, r0, r0
  60:	0b0b0024 	bleq	2c00f8 <_stack+0x2400f8>
  64:	08030b3e 	stmdaeq	r3, {r1, r2, r3, r4, r5, r8, r9, fp}
  68:	35080000 	strcc	r0, [r8, #-0]
  6c:	00134900 	andseq	r4, r3, r0, lsl #18
  70:	012e0900 			; <UNDEFINED> instruction: 0x012e0900
  74:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
  78:	0b3b0b3a 	bleq	ec2d68 <_stack+0xe42d68>
  7c:	01110b39 	tsteq	r1, r9, lsr fp
  80:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
  84:	01194296 			; <UNDEFINED> instruction: 0x01194296
  88:	0a000013 	beq	dc <minver_fabs-0x7f24>
  8c:	08030034 	stmdaeq	r3, {r2, r4, r5}
  90:	0b3b0b3a 	bleq	ec2d80 <_stack+0xe42d80>
  94:	13490b39 	movtne	r0, #39737	; 0x9b39
  98:	00001802 	andeq	r1, r0, r2, lsl #16
  9c:	3f012e0b 	svccc	0x00012e0b
  a0:	3a0e0319 	bcc	380d0c <_stack+0x300d0c>
  a4:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
  a8:	1113490b 	tstne	r3, fp, lsl #18
  ac:	40061201 	andmi	r1, r6, r1, lsl #4
  b0:	19429718 	stmdbne	r2, {r3, r4, r8, r9, sl, ip, pc}^
  b4:	00001301 	andeq	r1, r0, r1, lsl #6
  b8:	0300340c 	movweq	r3, #1036	; 0x40c
  bc:	3b0b3a0e 	blcc	2ce8fc <_stack+0x24e8fc>
  c0:	490b390b 	stmdbmi	fp, {r0, r1, r3, r8, fp, ip, sp}
  c4:	00180213 	andseq	r0, r8, r3, lsl r2
  c8:	012e0d00 			; <UNDEFINED> instruction: 0x012e0d00
  cc:	0e03193f 			; <UNDEFINED> instruction: 0x0e03193f
  d0:	0b3b0b3a 	bleq	ec2dc0 <_stack+0xe42dc0>
  d4:	01110b39 	tsteq	r1, r9, lsr fp
  d8:	18400612 	stmdane	r0, {r1, r4, r9, sl}^
  dc:	01194297 			; <UNDEFINED> instruction: 0x01194297
  e0:	0e000013 	mcreq	0, 0, r0, cr0, cr3, {0}
  e4:	193f012e 	ldmdbne	pc!, {r1, r2, r3, r5, r8}	; <UNPREDICTABLE>
  e8:	0b3a0e03 	bleq	e838fc <_stack+0xe038fc>
  ec:	0b390b3b 	bleq	e42de0 <_stack+0xdc2de0>
  f0:	13491927 	movtne	r1, #39207	; 0x9927
  f4:	06120111 			; <UNDEFINED> instruction: 0x06120111
  f8:	42961840 	addsmi	r1, r6, #64, 16	; 0x400000
  fc:	00130119 	andseq	r0, r3, r9, lsl r1
 100:	00050f00 	andeq	r0, r5, r0, lsl #30
 104:	0b3a0e03 	bleq	e83918 <_stack+0xe03918>
 108:	0b390b3b 	bleq	e42dfc <_stack+0xdc2dfc>
 10c:	18021349 	stmdane	r2, {r0, r3, r6, r8, r9, ip}
 110:	05100000 	ldreq	r0, [r0, #-0]
 114:	3a080300 	bcc	200d1c <_stack+0x180d1c>
 118:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
 11c:	0213490b 	andseq	r4, r3, #180224	; 0x2c000
 120:	11000018 	tstne	r0, r8, lsl r0
 124:	13490021 	movtne	r0, #36897	; 0x9021
 128:	0000052f 	andeq	r0, r0, pc, lsr #10
 12c:	3f012e12 	svccc	0x00012e12
 130:	3a0e0319 	bcc	380d9c <_stack+0x300d9c>
 134:	390b3b0b 	stmdbcc	fp, {r0, r1, r3, r8, r9, fp, ip, sp}
 138:	4919270b 	ldmdbmi	r9, {r0, r1, r3, r8, r9, sl, sp}
 13c:	12011113 	andne	r1, r1, #-1073741820	; 0xc0000004
 140:	97184006 	ldrls	r4, [r8, -r6]
 144:	13011942 	movwne	r1, #6466	; 0x1942
 148:	2e130000 	cdpcs	0, 1, cr0, cr3, cr0, {0}
 14c:	03193f01 	tsteq	r9, #1, 30
 150:	3b0b3a0e 	blcc	2ce990 <_stack+0x24e990>
 154:	270b390b 	strcs	r3, [fp, -fp, lsl #18]
 158:	11134919 	tstne	r3, r9, lsl r9
 15c:	40061201 	andmi	r1, r6, r1, lsl #4
 160:	19429718 	stmdbne	r2, {r3, r4, r8, r9, sl, ip, pc}^
 164:	Address 0x0000000000000164 is out of bounds.


Disassembly of section .debug_line:

00000000 <.debug_line>:
   0:	00000397 	muleq	r0, r7, r3
   4:	001f0003 	andseq	r0, pc, r3
   8:	01020000 	mrseq	r0, (UNDEF: 2)
   c:	000d0efb 	strdeq	r0, [sp], -fp
  10:	01010101 	tsteq	r1, r1, lsl #2
  14:	01000000 	mrseq	r0, (UNDEF: 0)
  18:	00010000 	andeq	r0, r1, r0
  1c:	766e696d 	strbtvc	r6, [lr], -sp, ror #18
  20:	632e7265 			; <UNDEFINED> instruction: 0x632e7265
  24:	00000000 	andeq	r0, r0, r0
  28:	00010500 	andeq	r0, r1, r0, lsl #10
  2c:	80000205 	andhi	r0, r0, r5, lsl #4
  30:	3d030000 	stccc	0, cr0, [r3, #-0]
  34:	85060501 	strhi	r0, [r6, #-1281]	; 0xfffffaff
  38:	a0830705 	addge	r0, r3, r5, lsl #14
  3c:	05670a05 	strbeq	r0, [r7, #-2565]!	; 0xfffff5fb
  40:	05866701 	streq	r6, [r6, #1793]	; 0x701
  44:	054bda09 	strbeq	sp, [fp, #-2569]	; 0xfffff5f7
  48:	12054c06 	andne	r4, r5, #1536	; 0x600
  4c:	01040200 	mrseq	r0, R12_usr
  50:	001f0566 	andseq	r0, pc, r6, ror #10
  54:	66020402 	strvs	r0, [r2], -r2, lsl #8
  58:	02002c05 	andeq	r2, r0, #1280	; 0x500
  5c:	05660304 	strbeq	r0, [r6, #-772]!	; 0xfffffcfc
  60:	0b05830c 	bleq	160c98 <_stack+0xe0c98>
  64:	4a03054d 	bmi	c15a0 <_stack+0x415a0>
  68:	05300d05 	ldreq	r0, [r0, #-3333]!	; 0xfffff2fb
  6c:	09054a05 	stmdbeq	r5, {r0, r2, r9, fp, lr}
  70:	840f052f 	strhi	r0, [pc], #-1327	; 78 <minver_fabs-0x7f88>
  74:	054a0705 	strbeq	r0, [sl, #-1797]	; 0xfffff8fb
  78:	0402001b 	streq	r0, [r2], #-27	; 0xffffffe5
  7c:	30052f03 	andcc	r2, r5, r3, lsl #30
  80:	03040200 	movweq	r0, #16896	; 0x4200
  84:	21053c08 	tstcs	r5, r8, lsl #24
  88:	03040200 	movweq	r0, #16896	; 0x4200
  8c:	0b053c08 	bleq	14f0b4 <_stack+0xcf0b4>
  90:	03040200 	movweq	r0, #16896	; 0x4200
  94:	0020052e 	eoreq	r0, r0, lr, lsr #10
  98:	65030402 	strvs	r0, [r3, #-1026]	; 0xfffffbfe
  9c:	02000705 	andeq	r0, r0, #1310720	; 0x140000
  a0:	05660104 	strbeq	r0, [r6, #-260]!	; 0xfffffefc
  a4:	0402001a 	streq	r0, [r2], #-26	; 0xffffffe6
  a8:	1e058502 	cfsh32ne	mvfx8, mvfx5, #2
  ac:	02040200 	andeq	r0, r4, #0, 4
  b0:	74087a03 	strvc	r7, [r8], #-2563	; 0xfffff5fd
  b4:	02000505 	andeq	r0, r0, #20971520	; 0x1400000
  b8:	05660104 	strbeq	r0, [r6, #-260]!	; 0xfffffefc
  bc:	0402001c 	streq	r0, [r2], #-28	; 0xffffffe4
  c0:	03058002 	movweq	r8, #20482	; 0x5002
  c4:	01040200 	mrseq	r0, R12_usr
  c8:	030a0566 	movweq	r0, #42342	; 0xa566
  cc:	0105820c 	tsteq	r5, ip, lsl #4
  d0:	0705f630 	smuadxeq	r5, r0, r6
  d4:	4b0a05d9 	blmi	281840 <_stack+0x201840>
  d8:	05840605 	streq	r0, [r4, #1541]	; 0x605
  dc:	04020011 	streq	r0, [r2], #-17	; 0xffffffef
  e0:	1f056601 	svcne	0x00056601
  e4:	02040200 	andeq	r0, r4, #0, 4
  e8:	bb0c0566 	bllt	301688 <_stack+0x281688>
  ec:	054b0605 	strbeq	r0, [fp, #-1541]	; 0xfffff9fb
  f0:	0305840b 	movweq	r8, #21515	; 0x540b
  f4:	000f054a 	andeq	r0, pc, sl, asr #10
  f8:	2f030402 	svccs	0x00030402
  fc:	02001b05 	andeq	r1, r0, #5120	; 0x1400
 100:	05b90304 	ldreq	r0, [r9, #772]!	; 0x304
 104:	04020003 	streq	r0, [r2], #-3
 108:	0b056601 	bleq	159914 <_stack+0xd9914>
 10c:	4a030585 	bmi	c1728 <_stack+0x41728>
 110:	052f0a05 	streq	r0, [pc, #-2565]!	; fffff713 <_stack+0xfff7f713>
 114:	0505840d 	streq	r8, [r5, #-1037]	; 0xfffffbf3
 118:	2f0b054a 	svccs	0x000b054a
 11c:	91080a05 	tstls	r8, r5, lsl #20
 120:	059f0e05 	ldreq	r0, [pc, #3589]	; f2d <minver_fabs-0x70d3>
 124:	1d05830b 	stcne	3, cr8, [r5, #-44]	; 0xffffffd4
 128:	02040200 	andeq	r0, r4, #0, 4
 12c:	00050546 	andeq	r0, r5, r6, asr #10
 130:	66010402 	strvs	r0, [r1], -r2, lsl #8
 134:	08890b05 	stmeq	r9, {r0, r2, r8, r9, fp}
 138:	67080575 	smlsdxvs	r8, r5, r5, r0
 13c:	05d71205 	ldrbeq	r1, [r7, #517]	; 0x205
 140:	0805830e 	stmdaeq	r5, {r1, r2, r3, r8, r9, pc}
 144:	0a05834c 	beq	160e7c <_stack+0xe0e7c>
 148:	17056783 	strne	r6, [r5, -r3, lsl #15]
 14c:	9e1105bb 	cfcmp64ls	r0, mvdx1, mvdx11
 150:	bc0f059f 	cfstr32lt	mvfx0, [pc], {159}	; 0x9f
 154:	054a0705 	strbeq	r0, [sl, #-1797]	; 0xfffff8fb
 158:	0402000b 	streq	r0, [r2], #-11
 15c:	2b052f03 	blcs	14bd70 <_stack+0xcbd70>
 160:	03040200 	movweq	r0, #16896	; 0x4200
 164:	1c057508 	cfstr32ne	mvfx7, [r5], {8}
 168:	03040200 	movweq	r0, #16896	; 0x4200
 16c:	02003c08 	andeq	r3, r0, #8, 24	; 0x800
 170:	3d080304 	stccc	3, cr0, [r8, #-16]
 174:	02001f05 	andeq	r1, r0, #5, 30
 178:	71080304 	tstvc	r8, r4, lsl #6
 17c:	02000705 	andeq	r0, r0, #1310720	; 0x140000
 180:	05660104 	strbeq	r0, [r6, #-260]!	; 0xfffffefc
 184:	0505890d 	streq	r8, [r5, #-2317]	; 0xfffff6f3
 188:	001a054a 	andseq	r0, sl, sl, asr #10
 18c:	2f030402 	svccs	0x00030402
 190:	02001d05 	andeq	r1, r0, #320	; 0x140
 194:	2c020304 	stccs	3, cr0, [r2], {4}
 198:	00050511 	andeq	r0, r5, r1, lsl r5
 19c:	66010402 	strvs	r0, [r1], -r2, lsl #8
 1a0:	05850d05 	streq	r0, [r5, #3333]	; 0xd05
 1a4:	0a054a05 	beq	1529c0 <_stack+0xd29c0>
 1a8:	830b052f 	movwhi	r0, #46383	; 0xb52f
 1ac:	75080c05 	strvc	r0, [r8, #-3077]	; 0xfffff3fb
 1b0:	05841305 	streq	r1, [r4, #773]	; 0x305
 1b4:	10054a0b 	andne	r4, r5, fp, lsl #20
 1b8:	002e052f 	eoreq	r0, lr, pc, lsr #10
 1bc:	82010402 	andhi	r0, r1, #33554432	; 0x2000000
 1c0:	02004205 	andeq	r4, r0, #1342177280	; 0x50000000
 1c4:	3c080104 	stfccs	f0, [r8], {4}
 1c8:	02003305 	andeq	r3, r0, #335544320	; 0x14000000
 1cc:	3c080104 	stfccs	f0, [r8], {4}
 1d0:	02002e05 	andeq	r2, r0, #5, 28	; 0x50
 1d4:	054a0104 	strbeq	r0, [sl, #-260]	; 0xfffffefc
 1d8:	04020023 	streq	r0, [r2], #-35	; 0xffffffdd
 1dc:	05570802 	ldrbeq	r0, [r7, #-2050]	; 0xfffff7fe
 1e0:	0402000b 	streq	r0, [r2], #-11
 1e4:	20056601 	andcs	r6, r5, r1, lsl #12
 1e8:	4a230585 	bmi	8c1804 <_stack+0x841804>
 1ec:	054a1e05 	strbeq	r1, [sl, #-3589]	; 0xfffff1fb
 1f0:	0402001d 	streq	r0, [r2], #-29	; 0xffffffe3
 1f4:	08780302 	ldmdaeq	r8!, {r1, r8, r9}^
 1f8:	0005053c 	andeq	r0, r5, ip, lsr r5
 1fc:	66010402 	strvs	r0, [r1], -r2, lsl #8
 200:	02001e05 	andeq	r1, r0, #5, 28	; 0x50
 204:	0d030204 	sfmeq	f0, 4, [r3, #-16]
 208:	00180582 	andseq	r0, r8, r2, lsl #11
 20c:	66020402 	strvs	r0, [r2], -r2, lsl #8
 210:	02001b05 	andeq	r1, r0, #5120	; 0x1400
 214:	52030204 	andpl	r0, r3, #4, 4	; 0x40000000
 218:	0003059e 	muleq	r3, lr, r5
 21c:	66010402 	strvs	r0, [r1], -r2, lsl #8
 220:	31030b05 	tstcc	r3, r5, lsl #22
 224:	4a030582 	bmi	c1834 <_stack+0x41834>
 228:	05310905 	ldreq	r0, [r1, #-2309]!	; 0xfffff6fb
 22c:	0583bb0a 	streq	fp, [r3, #2826]	; 0xb0a
 230:	1105bb17 	tstne	r5, r7, lsl fp
 234:	0f059f9e 	svceq	0x00059f9e
 238:	4a0705bc 	bmi	1c1930 <_stack+0x141930>
 23c:	02000b05 	andeq	r0, r0, #5120	; 0x1400
 240:	052f0304 	streq	r0, [pc, #-772]!	; ffffff44 <_stack+0xfff7ff44>
 244:	0402002b 	streq	r0, [r2], #-43	; 0xffffffd5
 248:	05750803 	ldrbeq	r0, [r5, #-2051]!	; 0xfffff7fd
 24c:	0402001c 	streq	r0, [r2], #-28	; 0xffffffe4
 250:	02009e03 	andeq	r9, r0, #3, 28	; 0x30
 254:	3d080304 	stccc	3, cr0, [r8, #-16]
 258:	02001f05 	andeq	r1, r0, #5, 30
 25c:	05d30304 	ldrbeq	r0, [r3, #772]	; 0x304
 260:	04020007 	streq	r0, [r2], #-7
 264:	09056601 	stmdbeq	r5, {r0, r9, sl, sp, lr}
 268:	05827a03 	streq	r7, [r2, #2563]	; 0xa03
 26c:	06052f07 	streq	r2, [r5], -r7, lsl #30
 270:	052e0b03 	streq	r0, [lr, #-2819]!	; 0xfffff4fd
 274:	04020003 	streq	r0, [r2], #-3
 278:	66710301 	ldrbtvs	r0, [r1], -r1, lsl #6
 27c:	11030e05 	tstne	r3, r5, lsl #28
 280:	830a0582 	movwhi	r0, #42370	; 0xa582
 284:	02000105 	andeq	r0, r0, #1073741825	; 0x40000001
 288:	f9300104 			; <UNDEFINED> instruction: 0xf9300104
 28c:	05681005 	strbeq	r1, [r8, #-5]!
 290:	03054d0b 	movweq	r4, #23819	; 0x5d0b
 294:	300d054a 	andcc	r0, sp, sl, asr #10
 298:	054a0505 	strbeq	r0, [sl, #-1285]	; 0xfffffafb
 29c:	04020007 	streq	r0, [r2], #-7
 2a0:	1a052f03 	bne	14beb4 <_stack+0xcbeb4>
 2a4:	03040200 	movweq	r0, #16896	; 0x4200
 2a8:	0402002e 	streq	r0, [r2], #-46	; 0xffffffd2
 2ac:	112e0203 			; <UNDEFINED> instruction: 0x112e0203
 2b0:	02000505 	andeq	r0, r0, #20971520	; 0x1400000
 2b4:	05660104 	strbeq	r0, [r6, #-260]!	; 0xfffffefc
 2b8:	04020018 	streq	r0, [r2], #-24	; 0xffffffe8
 2bc:	03056402 	movweq	r6, #21506	; 0x5402
 2c0:	01040200 	mrseq	r0, R12_usr
 2c4:	6b010566 	blvs	41864 <__bss_end__+0x28aa4>
 2c8:	680a05be 	stmdavs	sl, {r1, r2, r3, r4, r5, r7, r8, sl}
 2cc:	05850b05 	streq	r0, [r5, #2821]	; 0xb05
 2d0:	0d054a03 	vstreq	s8, [r5, #-12]
 2d4:	4a050530 	bmi	14179c <_stack+0xc179c>
 2d8:	02002305 	andeq	r2, r0, #335544320	; 0x14000000
 2dc:	052f0304 	streq	r0, [pc, #-772]!	; ffffffe0 <_stack+0xfff7ffe0>
 2e0:	04020011 	streq	r0, [r2], #-17	; 0xffffffef
 2e4:	053c0803 	ldreq	r0, [ip, #-2051]!	; 0xfffff7fd
 2e8:	0402001a 	streq	r0, [r2], #-26	; 0xffffffe6
 2ec:	05056503 	streq	r6, [r5, #-1283]	; 0xfffffafd
 2f0:	01040200 	mrseq	r0, R12_usr
 2f4:	00180566 	andseq	r0, r8, r6, ror #10
 2f8:	64020402 	strvs	r0, [r2], #-1026	; 0xfffffbfe
 2fc:	02000305 	andeq	r0, r0, #335544320	; 0x14000000
 300:	05660104 	strbeq	r0, [r6, #-260]!	; 0xfffffefc
 304:	0a056c1d 	beq	15b380 <_stack+0xdb380>
 308:	4a250566 	bmi	9418a8 <_stack+0x8c18a8>
 30c:	03830105 	orreq	r0, r3, #1073741825	; 0x40000001
 310:	0705d609 	streq	sp, [r5, -r9, lsl #12]
 314:	840b0569 	strhi	r0, [fp], #-1385	; 0xfffffa97
 318:	054a0305 	strbeq	r0, [sl, #-773]	; 0xfffffcfb
 31c:	0505300d 	streq	r3, [r5, #-13]
 320:	002a054a 	eoreq	r0, sl, sl, asr #10
 324:	2f030402 	svccs	0x00030402
 328:	02001b05 	andeq	r1, r0, #5120	; 0x1400
 32c:	3c080304 	stccc	3, cr0, [r8], {4}
 330:	02001a05 	andeq	r1, r0, #20480	; 0x5000
 334:	3b080304 	blcc	200f4c <_stack+0x180f4c>
 338:	02000505 	andeq	r0, r0, #20971520	; 0x1400000
 33c:	05660104 	strbeq	r0, [r6, #-260]!	; 0xfffffefc
 340:	04020018 	streq	r0, [r2], #-24	; 0xffffffe8
 344:	03056402 	movweq	r6, #21506	; 0x5402
 348:	01040200 	mrseq	r0, R12_usr
 34c:	0b056c66 	bleq	15b4ec <_stack+0xdb4ec>
 350:	4a030568 	bmi	c18f8 <_stack+0x418f8>
 354:	05300d05 	ldreq	r0, [r0, #-3333]!	; 0xfffff2fb
 358:	2b054a05 	blcs	152b74 <_stack+0xd2b74>
 35c:	03040200 	movweq	r0, #16896	; 0x4200
 360:	001c052f 	andseq	r0, ip, pc, lsr #10
 364:	08030402 	stmdaeq	r3, {r1, sl}
 368:	001a053c 	andseq	r0, sl, ip, lsr r5
 36c:	08030402 	stmdaeq	r3, {r1, sl}
 370:	0005053b 	andeq	r0, r5, fp, lsr r5
 374:	66010402 	strvs	r0, [r1], -r2, lsl #8
 378:	02001805 	andeq	r1, r0, #327680	; 0x50000
 37c:	05640204 	strbeq	r0, [r4, #-516]!	; 0xfffffdfc
 380:	04020003 	streq	r0, [r2], #-3
 384:	056c6601 	strbeq	r6, [ip, #-1537]!	; 0xfffff9ff
 388:	24089f01 	strcs	r9, [r8], #-3841	; 0xfffff0ff
 38c:	2f4b0305 	svccs	0x004b0305
 390:	05300c05 	ldreq	r0, [r0, #-3077]!	; 0xfffff3fb
 394:	08024b01 	stmdaeq	r2, {r0, r8, r9, fp, lr}
 398:	Address 0x0000000000000398 is out of bounds.


Disassembly of section .debug_frame:

00000000 <.debug_frame>:
   0:	0000000c 	andeq	r0, r0, ip
   4:	ffffffff 			; <UNDEFINED> instruction: 0xffffffff
   8:	7c020001 	stcvc	0, cr0, [r2], {1}
   c:	000d0c0e 	andeq	r0, sp, lr, lsl #24
  10:	0000001c 	andeq	r0, r0, ip, lsl r0
  14:	00000000 	andeq	r0, r0, r0
  18:	00008000 	andeq	r8, r0, r0
  1c:	0000005c 	andeq	r0, r0, ip, asr r0
  20:	8b040e42 	blhi	103930 <_stack+0x83930>
  24:	0b0d4201 	bleq	350830 <_stack+0x2d0830>
  28:	420d0d66 	andmi	r0, sp, #6528	; 0x1980
  2c:	00000ecb 	andeq	r0, r0, fp, asr #29
  30:	0000001c 	andeq	r0, r0, ip, lsl r0
  34:	00000000 	andeq	r0, r0, r0
  38:	0000805c 	andeq	r8, r0, ip, asr r0
  3c:	000001a4 	andeq	r0, r0, r4, lsr #3
  40:	8b040e42 	blhi	103950 <_stack+0x83950>
  44:	0b0d4201 	bleq	350850 <_stack+0x2d0850>
  48:	0d0dc202 	sfmeq	f4, 1, [sp, #-8]
  4c:	000ecb42 	andeq	ip, lr, r2, asr #22
  50:	00000024 	andeq	r0, r0, r4, lsr #32
  54:	00000000 	andeq	r0, r0, r0
  58:	00008200 	andeq	r8, r0, r0, lsl #4
  5c:	000006e8 	andeq	r0, r0, r8, ror #13
  60:	8b080e42 	blhi	203970 <_stack+0x183970>
  64:	42018e02 	andmi	r8, r1, #2, 28
  68:	03040b0c 	movweq	r0, #19212	; 0x4b0c
  6c:	0d0c0364 	stceq	3, cr0, [ip, #-400]	; 0xfffffe70
  70:	cbce4208 	blgt	ff390898 <_stack+0xff310898>
  74:	0000000e 	andeq	r0, r0, lr
  78:	0000001c 	andeq	r0, r0, ip, lsl r0
  7c:	00000000 	andeq	r0, r0, r0
  80:	000088e8 	andeq	r8, r0, r8, ror #17
  84:	000000d4 	ldrdeq	r0, [r0], -r4
  88:	8b040e42 	blhi	103998 <_stack+0x83998>
  8c:	0b0d4201 	bleq	350898 <_stack+0x2d0898>
  90:	0d0d6002 	stceq	0, cr6, [sp, #-8]
  94:	000ecb42 	andeq	ip, lr, r2, asr #22
  98:	0000001c 	andeq	r0, r0, ip, lsl r0
  9c:	00000000 	andeq	r0, r0, r0
  a0:	000089bc 			; <UNDEFINED> instruction: 0x000089bc
  a4:	000000d8 	ldrdeq	r0, [r0], -r8
  a8:	8b040e42 	blhi	1039b8 <_stack+0x839b8>
  ac:	0b0d4201 	bleq	3508b8 <_stack+0x2d08b8>
  b0:	0d0d5e02 	stceq	14, cr5, [sp, #-8]
  b4:	000ecb42 	andeq	ip, lr, r2, asr #22
  b8:	00000024 	andeq	r0, r0, r4, lsr #32
  bc:	00000000 	andeq	r0, r0, r0
  c0:	00008a94 	muleq	r0, r4, sl
  c4:	00000190 	muleq	r0, r0, r1
  c8:	8b080e42 	blhi	2039d8 <_stack+0x1839d8>
  cc:	42018e02 	andmi	r8, r1, #2, 28
  d0:	02040b0c 	andeq	r0, r4, #12, 22	; 0x3000
  d4:	080d0cb6 	stmdaeq	sp, {r1, r2, r4, r5, r7, sl, fp}
  d8:	0ecbce42 	cdpeq	14, 12, cr12, cr11, cr2, {2}
  dc:	00000000 	andeq	r0, r0, r0
  e0:	00000020 	andeq	r0, r0, r0, lsr #32
  e4:	00000000 	andeq	r0, r0, r0
  e8:	00008c24 	andeq	r8, r0, r4, lsr #24
  ec:	00000028 	andeq	r0, r0, r8, lsr #32
  f0:	8b080e42 	blhi	203a00 <_stack+0x183a00>
  f4:	42018e02 	andmi	r8, r1, #2, 28
  f8:	4c040b0c 			; <UNDEFINED> instruction: 0x4c040b0c
  fc:	42080d0c 	andmi	r0, r8, #12, 26	; 0x300
 100:	000ecbce 	andeq	ip, lr, lr, asr #23

Disassembly of section .debug_str:

00000000 <.debug_str>:
   0:	20554e47 	subscs	r4, r5, r7, asr #28
   4:	20373143 	eorscs	r3, r7, r3, asr #2
   8:	332e3031 			; <UNDEFINED> instruction: 0x332e3031
   c:	3220312e 	eorcc	r3, r0, #-2147483637	; 0x8000000b
  10:	30313230 	eorscc	r3, r1, r0, lsr r2
  14:	20313236 	eorscs	r3, r1, r6, lsr r2
  18:	6c657228 	sfmvs	f7, 2, [r5], #-160	; 0xffffff60
  1c:	65736165 	ldrbvs	r6, [r3, #-357]!	; 0xfffffe9b
  20:	6d2d2029 	stcvs	0, cr2, [sp, #-164]!	; 0xffffff5c
  24:	656e7574 	strbvs	r7, [lr, #-1396]!	; 0xfffffa8c
  28:	726f633d 	rsbvc	r6, pc, #-201326592	; 0xf4000000
  2c:	2d786574 	cfldr64cs	mvdx6, [r8, #-464]!	; 0xfffffe30
  30:	2d203861 	stccs	8, cr3, [r0, #-388]!	; 0xfffffe7c
  34:	7570666d 	ldrbvc	r6, [r0, #-1645]!	; 0xfffff993
  38:	6f656e3d 	svcvs	0x00656e3d
  3c:	6d2d206e 	stcvs	0, cr2, [sp, #-440]!	; 0xfffffe48
  40:	616f6c66 	cmnvs	pc, r6, ror #24
  44:	62612d74 	rsbvs	r2, r1, #116, 26	; 0x1d00
  48:	61683d69 	cmnvs	r8, r9, ror #26
  4c:	2d206472 	cfstrscs	mvf6, [r0, #-456]!	; 0xfffffe38
  50:	7570636d 	ldrbvc	r6, [r0, #-877]!	; 0xfffffc93
  54:	6d72613d 	ldfvse	f6, [r2, #-244]!	; 0xffffff0c
  58:	6d647437 	cfstrdvs	mvd7, [r4, #-220]!	; 0xffffff24
  5c:	6d2d2069 	stcvs	0, cr2, [sp, #-420]!	; 0xfffffe5c
  60:	206d7261 	rsbcs	r7, sp, r1, ror #4
  64:	72616d2d 	rsbvc	r6, r1, #2880	; 0xb40
  68:	613d6863 	teqvs	sp, r3, ror #16
  6c:	34766d72 	ldrbtcc	r6, [r6], #-3442	; 0xfffff28e
  70:	672d2074 			; <UNDEFINED> instruction: 0x672d2074
  74:	304f2d20 	subcc	r2, pc, r0, lsr #26
  78:	726f7700 	rsbvc	r7, pc, #0, 14
  7c:	6970006b 	ldmdbvs	r0!, {r0, r1, r3, r5, r6}^
  80:	00746f76 	rsbseq	r6, r4, r6, ror pc
  84:	766e696d 	strbtvc	r6, [lr], -sp, ror #18
  88:	6d5f7265 	lfmvs	f7, 2, [pc, #-404]	; fffffefc <_stack+0xfff7fefc>
  8c:	006e6961 	rsbeq	r6, lr, r1, ror #18
  90:	766e696d 	strbtvc	r6, [lr], -sp, ror #18
  94:	615f7265 	cmpvs	pc, r5, ror #4
  98:	696d0061 	stmdbvs	sp!, {r0, r5, r6}^
  9c:	7265766e 	rsbvc	r7, r5, #115343360	; 0x6e00000
  a0:	6d00615f 	stfvss	f6, [r0, #-380]	; 0xfffffe84
  a4:	65766e69 	ldrbvs	r6, [r6, #-3689]!	; 0xfffff197
  a8:	00625f72 	rsbeq	r5, r2, r2, ror pc
  ac:	766e696d 	strbtvc	r6, [lr], -sp, ror #18
  b0:	635f7265 	cmpvs	pc, #1342177286	; 0x50000006
  b4:	6e696d00 	cdpvs	13, 6, cr6, cr9, cr0, {0}
  b8:	5f726576 	svcpl	0x00726576
  bc:	00746564 	rsbseq	r6, r4, r4, ror #10
  c0:	5f6c6f63 	svcpl	0x006c6f63
  c4:	696d0062 	stmdbvs	sp!, {r1, r5, r6}^
  c8:	7265766e 	rsbvc	r7, r5, #115343360	; 0x6e00000
  cc:	756d6d5f 	strbvc	r6, [sp, #-3423]!	; 0xfffff2a1
  d0:	696d006c 	stmdbvs	sp!, {r2, r3, r5, r6}^
  d4:	7265766e 	rsbvc	r7, r5, #115343360	; 0x6e00000
  d8:	6261665f 	rsbvs	r6, r1, #99614720	; 0x5f00000
  dc:	68630073 	stmdavs	r3!, {r0, r1, r4, r5, r6}^
  e0:	5f6b6365 	svcpl	0x006b6365
  e4:	006d7573 	rsbeq	r7, sp, r3, ror r5
  e8:	766e696d 	strbtvc	r6, [lr], -sp, ror #18
  ec:	632e7265 			; <UNDEFINED> instruction: 0x632e7265
  f0:	64697300 	strbtvs	r7, [r9], #-768	; 0xfffffd00
  f4:	6f640065 	svcvs	0x00640065
  f8:	656c6275 	strbvs	r6, [ip, #-629]!	; 0xfffffd8b
  fc:	6f682f00 	svcvs	0x00682f00
 100:	732f656d 			; <UNDEFINED> instruction: 0x732f656d
 104:	72646e61 	rsbvc	r6, r4, #1552	; 0x610
 108:	6f442f6f 	svcvs	0x00442f6f
 10c:	656d7563 	strbvs	r7, [sp, #-1379]!	; 0xfffffa9d
 110:	2f73746e 	svccs	0x0073746e
 114:	6b726f57 	blvs	1c9be78 <_stack+0x1c1be78>
 118:	6e65422f 	cdpvs	2, 6, cr4, cr5, cr15, {1}
 11c:	616d6863 	cmnvs	sp, r3, ror #16
 120:	2f736b72 	svccs	0x00736b72
 124:	6c636154 	stfvse	f6, [r3], #-336	; 0xfffffeb0
 128:	6e654265 	cdpvs	2, 6, cr4, cr5, cr5, {3}
 12c:	6d2f6863 	stcvs	8, cr6, [pc, #-396]!	; ffffffa8 <_stack+0xfff7ffa8>
 130:	65766e69 	ldrbvs	r6, [r6, #-3689]!	; 0xfffff197
 134:	72732f72 	rsbsvc	r2, r3, #456	; 0x1c8
 138:	6f720063 	svcvs	0x00720063
 13c:	00615f77 	rsbeq	r5, r1, r7, ror pc
 140:	5f776f72 	svcpl	0x00776f72
 144:	6f720062 	svcvs	0x00720062
 148:	00635f77 	rsbeq	r5, r3, r7, ror pc
 14c:	766e696d 	strbtvc	r6, [lr], -sp, ror #18
 150:	6d5f7265 	lfmvs	f7, 2, [pc, #-404]	; ffffffc4 <_stack+0xfff7ffc4>
 154:	65766e69 	ldrbvs	r6, [r6, #-3689]!	; 0xfffff197
 158:	6d770072 	ldclvs	0, cr0, [r7, #-456]!	; 0xfffffe38
 15c:	6d007861 	stcvs	8, cr7, [r0, #-388]	; 0xfffffe7c
 160:	65766e69 	ldrbvs	r6, [r6, #-3689]!	; 0xfffff197
 164:	6e695f72 	mcrvs	15, 3, r5, cr9, cr2, {3}
 168:	63007469 	movwvs	r7, #1129	; 0x469
 16c:	615f6c6f 	cmpvs	pc, pc, ror #24
 170:	6e696d00 	cdpvs	13, 6, cr6, cr9, cr0, {0}
 174:	5f726576 	svcpl	0x00726576
 178:	00695f61 	rsbeq	r5, r9, r1, ror #30
 17c:	69736e75 	ldmdbvs	r3!, {r0, r2, r4, r5, r6, r9, sl, fp, sp, lr}^
 180:	64656e67 	strbtvs	r6, [r5], #-3687	; 0xfffff199
 184:	746e6920 	strbtvc	r6, [lr], #-2336	; 0xfffff6e0
 188:	6c6f6300 	stclvs	3, cr6, [pc], #-0	; 190 <minver_fabs-0x7e70>
 18c:	6d00635f 	stcvs	3, cr6, [r0, #-380]	; 0xfffffe84
 190:	65766e69 	ldrbvs	r6, [r6, #-3689]!	; 0xfffff197
 194:	65725f72 	ldrbvs	r5, [r2, #-3954]!	; 0xfffff08e
 198:	6e727574 	mrcvs	5, 3, r7, cr2, cr4, {3}
	...

Disassembly of section .ARM.attributes:

00000000 <.ARM.attributes>:
   0:	00002f41 	andeq	r2, r0, r1, asr #30
   4:	61656100 	cmnvs	r5, r0, lsl #2
   8:	01006962 	tsteq	r0, r2, ror #18
   c:	00000025 	andeq	r0, r0, r5, lsr #32
  10:	00543405 	subseq	r3, r4, r5, lsl #8
  14:	01080206 	tsteq	r8, r6, lsl #4
  18:	030a0109 	movweq	r0, #41225	; 0xa109
  1c:	0412010c 	ldreq	r0, [r2], #-268	; 0xfffffef4
  20:	01150114 	tsteq	r5, r4, lsl r1
  24:	01180317 	tsteq	r8, r7, lsl r3
  28:	011a0119 	tsteq	sl, r9, lsl r1
  2c:	061e011c 			; <UNDEFINED> instruction: 0x061e011c
