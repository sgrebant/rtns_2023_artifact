TARGET=dist
GOAL=.
if [ $# -ne 0 ]
then
	GOAL=$*
fi

# remove existing dist directory
rm -rf $TARGET

# find elf and ff files
BINARIES=$(find $GOAL -name "*.elf")
FLOWFACTS=$(find $GOAL -name "*.ff")

# create the dist directory again
mkdir $TARGET

# copy files into a dist directory
cp $BINARIES $TARGET
cp $FLOWFACTS $TARGET
