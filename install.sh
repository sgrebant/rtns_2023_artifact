#!/bin/bash

### SETUP

# Dependencies
OPOLY="otawa-poly.zip"
ONORMAL="otawa.zip"

# Git repositories URLs
WSYMBURL="https://gitlab.cristal.univ-lille.fr/otawa-plugins/WSymb.git"
POLYTABURL="https://gitlab.cristal.univ-lille.fr/otawa-plugins/polymalys.git"

# Git commits
WSYMBCM="bf076e130a0f7c7eb161828ab8ad94217ae6a899"
POLYTABCM="61610386ef5399b915748f9fde57e905c848964a"

### CHECK DEPENDENCIES

if [ ! -e $ONORMAL ]
then
    echo "Could not find $ONORMAL. Check that it is in the same directory"
    exit
fi

### SETUP SYSTEM

# Extract dependencies
unzip $ONORMAL

# Clone git repositories
git clone $WSYMBURL
git clone $POLYTABURL

# checkout correct version
DIR="$(pwd)"

WSYMBDIR="$DIR/WSymb"
POLYTABDIR="$DIR/polymalys"

cd $WSYMBDIR
git checkout $WSYMBCM
cd $DIR
cd $POLYTABDIR
git checkout $POLYTABCM

### BUILD ALL
ORIG_PATH=$PATH
ORIG_LD=$LD_LIBRARY_PATH

POLYPATH=$POLYTABDIR/otawa
NORMALPATH=$DIR/otawa

# Polymalys
export PATH=$ORIG_PATH:$POLYPATH/bin/
export LD_LIBRARY_PATH=$ORIG_LD:$POLYPATH/lib:$POLYPATH/lib/otawa/otawa/
cd $POLYTABDIR
make install
cd $POLYTABDIR/tests
make poly

# WSymb
export PATH=$ORIG_PATH:$NORMALPATH/bin/
export LD_LIBRARY_PATH=$ORIG_LD:$NORMALPATH/lib:$NORMALPATH/lib/otawa/otawa/
cd $WSYMBDIR
make install
cd $WSYMBDIR/simplify
make
cd $WSYMBDIR/libpwcet
make libpwcet.a

### Distribute benchmark programs
cd $DIR/Benchmarks
./dist.sh
cd $DIR

# Reset
cd $DIR
export PATH=$ORIG_PATH
export LD_LIBRARY_PATH=$ORIG_LD
