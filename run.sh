#!/bin/bash

### Check that we have two args
if [ $# -ne 2 ]
then
    echo "Usage: run.sh <program> <function>"
    exit
fi

DIR="$(pwd)"
DIST="$DIR/Benchmarks/dist"
PROGRAM=$1
FUNCTION=$2

EXP=$DIR/experiments
BUILD=$EXP/build

mkdir -p $EXP
rm -rf $BUILD
mkdir -p $BUILD
cd $BUILD

### Check that the benchmark program exists
if [ ! -e $DIST/$PROGRAM.elf ]
then
    echo "$DIST/$PROGRAM.elf not found"
    exit
fi
cp $DIST/$PROGRAM.elf .

if [ ! -e $DIST/$PROGRAM.ff ]
then
    echo "$DIST/$PROGRAM.ff not found"
    exit
fi
cp $DIST/$PROGRAM.ff .

### Run the experiments

ORIG_PATH=$PATH
ORIG_LD=$LD_LIBRARY_PATH

# 1. Polyhedra analysis
POLYTABDIR=$DIR/polymalys
POLYPATH=$POLYTABDIR/otawa
export PATH=$ORIG_PATH:$POLYPATH/bin/
export LD_LIBRARY_PATH=$ORIG_LD:$POLYPATH/lib:$POLYPATH/lib/otawa/otawa/
$POLYTABDIR/tests/poly $PROGRAM.elf $FUNCTION

# 2.1. Comparison with IPET
NORMALPATH=$DIR/otawa
export PATH=$ORIG_PATH:$NORMALPATH/bin/
export LD_LIBRARY_PATH=$ORIG_LD:$NORMALPATH/lib:$NORMALPATH/lib/otawa/otawa/
START=$(date +%s.%N)
$DIR/otawa/bin/owcet -s cmp_pipe -p pipeline=$DIR/WSymb/hw/processor.xml -p cache=$DIR/WSymb/hw/cache.xml $BUILD/$PROGRAM.elf $FUNCTION
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "IPET time: $DIFF s"

# 2.2. Symbolic WCET analysis
FORMULA=${PROGRAM}_-_$FUNCTION
$DIR/WSymb/dumpcft $PROGRAM.elf ${FORMULA}_BEFORE.pwf $FUNCTION

# 3. Reset path
export PATH=$ORIG_PATH
export LD_LIBRARY_PATH=$ORIG_LD

# 4. Formula simplification
INSTS=$EXP/instantiators
mkdir -p $INSTS
START=$(date +%s.%N)
$DIR/WSymb/simplify/swymplify "${FORMULA}_BEFORE.pwf" > "${FORMULA}_AFTER.pwf"
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Simplification time: $DIFF s"
$DIR/WSymb/simplify/swymplify -c -i -o "$INSTS/$FORMULA.c" "${FORMULA}_BEFORE.pwf"

# 5. Instantiator compilation
cd $INSTS
gcc -Ofast -g -Wall -static -march=native -I$DIR/WSymb/libpwcet/ -o $FORMULA $FORMULA.c  $DIR/WSymb/libpwcet/libpwcet.a
cd  $BUILD

# 6. Copy the resulting files (input conditionals, formulas before/after simplification)
FORMULAS=$EXP/formulas
POLYRES=$EXP/input_conditionals
mkdir -p $FORMULAS
mkdir -p $POLYRES
cp ${FORMULA}_BEFORE.pwf $FORMULAS/
cp ${FORMULA}_AFTER.pwf $FORMULAS/
cp loop_bounds.csv $POLYRES/${FORMULA}_-_loop_bounds.csv
cp constraints.csv $POLYRES/${FORMULA}_-_constraints.csv
