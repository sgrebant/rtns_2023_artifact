# RTNS 2023 Artifact

This repository enables to reproduce the experimental results of the
paper **WCET analysis with procedure arguments as parameters**,
accepted at the [RTNS 2023](https://rtns2023.cs.tu-dortmund.de/)
conference.

## 1. Repository content

- ``analyzer.py``: Computes formula sizes;
- ``Benchmarks``: Source and binary files for all the benchmark programs used in the paper;
- ``compiler``: Python script used by the formula to C compiler;
- ``install.sh``: Installation script;
- ``optimized_instantiator.sh``: A script to produced optimized formula instanciators;
- ``otawa.zip``: Otawa version used for Symbolic WCET computation;
- ``run_all.sh``: Runs the analysis on all the benchmarks;
- ``run.sh``: Run the analysis on a single benchmark.

This artifact relies on two other public repositories:
- The
  [WSymb](https://gitlab.cristal.univ-lille.fr/otawa-plugins/WSymb)
  repository (symbolic WCET computation prototype)
- The
  [POLYMALYS](https://gitlab.cristal.univ-lille.fr/otawa-plugins/polymalys)
  repository (abstract interpretation on binary programs)

## 3. Installation

The artifact was tested with Ubuntu 22.04 (amd64).

Two installation methods are detailed below:
- Using a docker image with all the correct package versions
  (recommended), see ``3.1``
- A manual installation, see ``3.2``

### 3.1 Installing dependencies using a Docker image (recommended)

This installation method requires
[Docker](https://www.docker.com/). It can be installed on Debian based
systems (e.g. Ubuntu) with:

```shell
sudo apt install docker.io
```

Install and run the docker image with:
```shell
sudo docker run -it clementballabriga/rtns_2023_artifact
```

Go to section ``3.3`` to continue.

Once experiments are complete, type ``exit`` to exit the docker image.

### 3.2 Installing dependencies on your own computer

The following software must be installed:
- Git (2.34.1)
- Make (4.3)
- g++ (11.3.0)
- Parma Polyhedra Library (1.28)
- gcc-arm-linux-gnueabi (11.3.0)
- ocaml (4.13.1)
- ocamlbuild (0.14.1)

You can install them using the following command (tested on Ubuntu
22.04, ISO pulled and installed on 03-22-2023):

```shell
sudo apt install git make g++ libppl-dev gcc-arm-linux-gnueabi ocaml ocamlbuild
```

⚠️ The install script (``install.sh``) installs some files
(``cftree.so``, ``cftree.eld``, ``poly.so`` and ``poly.eld``) in
``~/.otawa/proc/otawa``. The directory is created if it does not
exist.

### 3.3 Installing WCET analysis tools

Install the WCET tools and their dependencies:

```shell
chmod +x install.sh
./install.sh
```

Then, the two tools, POLYMALYS and WSymb, are installed under the
``polymalys`` and the ``WSymb`` directories with their source code.

## 4. Running the experiments

### 4.1 Execution times and WCET instantiation

The following command runs the experiments for all the benchmarks:

```shell
chmod +x run_all.sh
./run_all.sh
```

You can also run experiments for a specific procedure with:

```shell
chmod +x run.sh
./run.sh <program_name> <function_name>
```

The resulting WCET formulas, before and after simplification, are
located in ``experiments/formulas``. Programs to instantiate the
formulas are located in ``experiments/instantiators``.

The syntax of formulas is as follows:
- A WCET value is represented by a tuple (l:x;{a,b,c,...}) where
  ``l:x`` is the loop identifier (0 is the program top-level scope),
  ``{a,b,c,...}`` is the ordered list representing the successive WCET
  values;
- The WCET "sum" is represented by ``w1 + w2``;
- The WCET "maximum" is represented by ``w1 U w2``;
- The WCET power operator (for loops) is represented by ``(wb, we,
  l:x)^(bound)``, where ``wb`` is the WCET of the loop body, ``we``
  the WCET of the exit tree, ``l:x`` is a loop identifier and
  ``bound`` is the loop bound;
- The input conditional multiplication is represented by ``ic * w``,
  where ``ic`` is the input conditional and ``w`` is a WCET.
- ``b:x`` is the ``(x+1)``th procedure argument
- ``__top`` is equivalent to ``l:0`` (the root loop scope)
- ``loops:`` contains information about loops hierarchy, where
  elements of ``X _c Y`` form state that ``the loop X contains the loop Y``
- ``endl`` indicates the end of the formula

The instantiator C codes and binaries are generated in
``experiments/instantiators``. They are not optimized, so their C code
should be fairly readable (see Section ``4.3`` to instead produce
instantiators optimized for reduced execution times). You can print
the WCET of the analyzed procedure for the desired argument values as
follows:

```shell
cd experiments/instantiators
./<instantiator_program> <arg1> <arg2> <arg3> <arg4>
```

Note that you must pass 4 arguments even if the procedure has less
arguments. You can use any value when the function has no such
argument, or when the formula does not depend on this argument value.

In the array below, we provide argument values that yield the lowest
and highest WCET for each instantiator:

Program | Function | Lowest trigger | Highest trigger
---|---|---|---
audiobeam | audiobeam_adjust_delays | 0 0 0 0 | 0 15 0 0
audiobeam | audiobeam_calc_distances | 0 0 0 0 | 0 0 0 15
audiobeam | audiobeam_calculate_energy | 0 0 0 0 | 0 0 0 0
audiobeam | audiobeam_find_max_in_array | 0 0 0 0 | 0 15 0 0
audiobeam | audiobeam_find_min_in_arr | 0 0 0 0 | 0 15 0 0
audiobeam | audiobeam_wrapped_dec | 1 0 0 0 | 0 0 0 0
audiobeam | audiobeam_wrapped_dec_offset | 0 0 0 0 | 0 1 0 0
audiobeam | audiobeam_wrapped_inc | 0 1 0 0 | 0 0 0 0
audiobeam | audiobeam_wrapped_inc_offset | 0 0 0 0 | 0 1 0 0
cjpeg_wrbmp | cjpeg_wrbmp_write_colormap | 0 0 0 1 | 0 0 4 0
fft | fft_modff | 0 0 0 0 | 0 0 0 0
g723_enc | g723_enc_quan | 0 0 0 0 | 0 0 15 0
g723_enc | g723_enc_reconstruct | 0 -1 1 0 | 1 1 1 1
gsm_dec | gsm_dec_APCM_inverse_quantization | 0 0 33000 0 | 0 0 0 0
gsm_dec | gsm_dec_APCM_quantization_xmaxc_to_exp_mant | 0 0 0 0 | 20 0 0 0
gsm_dec | gsm_dec_asl | 0 20 0 0 | 0 -10 0 0
gsm_dec | gsm_dec_asr | 0 -20 0 0 | 0 -10 0 0
gsm_dec | gsm_dec_Long_Ter_Synthesis_Filtering | 0 0 0 0 | 0 40 0 0
gsm_dec | gsm_dec_sub | 0 33000 0 0 | 0 0 0 0
gsm_enc | gsm_enc_asl | 0 20 0 0 | 0 -10 0 0
gsm_enc | gsm_enc_asr | 0 -20 0 0 | 0 -10 0 0
gsm_enc | gsm_enc_div | -1 0 0 0 | 0 0 0 0
gsm_enc | gsm_enc_sub | 0 33000 0 0 | 0 0 0 0
lift | lift_do_impulse | 0 1 0 0 | 0 0 0 0
ludcmp | ludcmp_test | 0 0 0 0 | 5 0 0 0
minver | minver_minver | 0 0 0 0 | 3 0 0 0
minver | minver_mmul | 0 0 0 0 | 3 3 3 3
mpeg2 | mpeg2_dist2 | 0 0 0 1 | 0 0 0 0
ndes | ndes_getbit | 0 0 0 0 | 0 0 1 0
rjindael_dec | rijndael_dec_fseek | 0 0 0 0 | 0 0 2 0
rijndael_enc | rijndael_enc_fseek | 0 0 0 0 | 0 0 2 0

### 4.2 Formula sizes

The formula sizes are printed by script ``run_all.sh`` after it
produces the formulas. You can also also print the formula sizes
without rerunning the complete analysis as follows:

```shell
python3 analyzer.py
```

### 4.3 Optimized instantiators

*Optimized instantiators* are optimized to reduce their execution
time. To generate them and compute their WCET, use:

```shell
chmod +x optimized_instantiator.sh
./optimized_instantiators.sh
```

The source and binary (ARM32) of each instantiator can be found in
``experiments/optimized_instantiators``.

## 5. Reproducing paper results

After running the experiments above, experimental results of the paper
can be retrieved from the artifact.

### Results for Table I

1. ``IPET/WCET``, ``IPET/Time (s)``, ``CFT/Time (s)/Polyhedra`` and
   ``CFT/Time (s)/Symb WCET`` can be found in ``debug.log`` after
   executing the ``run.sh`` or ``run_all.sh`` (see section ``4.1``),
   for example you can find these lines for each program in the file:

  ```shell
  WCET[audiobeam_adjust_delays] = 9261 cycles # IPET/WCET column result
  IPET time: 1.352890455 s # IPET/Time (s) column
  Time elapsed: 906 # CFT/Time (s)/Polyhedra column, but in milliseconds here (in seconds in the paper)
  dumpcft time: 1.251608425
  Simplification time: .002419409 s # CFT/Time (s)/Symb WCET is the sum of these last two results
  ```

2. ``CFT/WCET (cyc)`` columns can be obtained by running instantiators
   generated in Section ``4.1`` with provided values. The ``Diff (%)``
   column is computed with the following formula:
   $((Highest - Lowest) / Highest) * 100$. Results in the paper are
   rounded after the first decimal for percentages.

### Results for Figure 8
Simply run:

```shell
python3 analyzer.py
```

It will compute the formula sizes from the formula files in
experiments/formulas

### Results for Table II

Simply run:

```shell
./optimized_instantiators.sh
```

It will compile the instantiators, compute and display their WCET (the
``Inst.`` column). The ``Max gain`` is $Highest - Lowest - Inst.$ (from Table I),
``WCET`` reports the ``CFT/WCET/Highest`` of Table I. ``Op`` is the result
computed for Figure 8.
