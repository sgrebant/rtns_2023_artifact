import re
import os

formulas_dir = "experiments/formulas/"
format_size = 70

"""A simple function to compute before and after formula sizes"""
def fsize():
    fs = [] # array of formula information
    regex = "[U+|.*=≤&^-]"
    files = os.listdir(formulas_dir)
    files.sort()
    i=0
    for file in files:
        fi = open(formulas_dir + file, "r")
        content = fi.read()
        splitted = re.split(regex, content)
        size = len(splitted)
        for e in splitted:
            # removes the second operand when it is an unary operator
            if len(e.replace(" ","")) == 0:
                size -= 1
        index = int(i/2)
        if index >= len(fs):
            fs.append([])
        f = fs[index]
        if len(f) == 0 or len(f) == 1:
            f.append(size)
        else:
            index += 1
            f = []
            f[0] = size
        fs[index] = f
        i += 1
    print("{}{}{}".format("Function".ljust(format_size), "\tBefore", "\tAfter"))
    i=0
    for file in files:
        if i%2 == 0:
            fileName = file.replace("_AFTER.pwf", "")
            print("{}{}{}".format(fileName.ljust(format_size), "\t" + str(fs[int(i/2)][1]), "\t" + str(fs[int(i/2)][0])))
        i += 1

fsize()
