#!/bin/bash

# Analysis progression
i=1
MAX=31

### A function to run each experiment of the paper
run () {
    echo "Analysing $1 $2 ($i/$MAX)"
    # print result in a log file
    {
	echo "=========================================================================== $1 $2"
	./run.sh $1 $2
    } >> debug.log 2>> debug.log
    i=$((i+1))
}

# Remove all data from previous run
rm -rf experiments

### Run all experiments
run audiobeam audiobeam_adjust_delays
run audiobeam audiobeam_calc_distances
run audiobeam audiobeam_calculate_energy
run audiobeam audiobeam_find_max_in_arr
run audiobeam audiobeam_find_min_in_arr
run audiobeam audiobeam_wrapped_dec
run audiobeam audiobeam_wrapped_dec_offset
run audiobeam audiobeam_wrapped_inc
run audiobeam audiobeam_wrapped_inc_offset

run cjpeg_wrbmp cjpeg_wrbmp_write_colormap

run fft fft_modff

run g723_enc g723_enc_quan
run g723_enc g723_enc_reconstruct

run gsm_dec gsm_dec_APCM_inverse_quantization
run gsm_dec gsm_dec_APCM_quantization_xmaxc_to_exp_mant
run gsm_dec gsm_dec_asl
run gsm_dec gsm_dec_asr
run gsm_dec gsm_dec_Long_Term_Synthesis_Filtering
run gsm_dec gsm_dec_sub

run gsm_enc gsm_enc_asl
run gsm_enc gsm_enc_asr
run gsm_enc gsm_enc_div
run gsm_enc gsm_enc_sub

run lift lift_do_impulse

run ludcmp ludcmp_test

run minver minver_minver
run minver minver_mmul

run mpeg2 mpeg2_dist2

run ndes ndes_getbit

run rijndael_dec rijndael_dec_fseek

run rijndael_enc rijndael_enc_fseek

# Display formula sizes comparisons
python3 analyzer.py
